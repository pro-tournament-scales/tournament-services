const unableToAuthMessage = "We were unable to authtenticate you. Please try again later."
/* verify angler scope */
function verifyAngler(req, res, next) {
    if(req.user == null || req.user.id == null || req.user.scope != "angler") {
      res.status(401).json({
        message: unableToAuthMessage
      });
      return;
    }
    next();
}

function verifyBetaParticipant(req, res, next) {
  if (req.user == null || req.user.id == null || req.user.scope != "beta") {
    res.status(401).json({
      message: unableToAuthMessage
    });
    return;
  }
  next();
}

/* verify angler scope */
function verifyAnglerLimited(req, res, next) {
    if(req.user == null || req.user.id == null || req.user.scope != "angler_limited") {
      res.status(401).json({
        message: unableToAuthMessage
      });
      return;
    }
    next();
}

/* verify boat captain scope */
function verifyBoatCaptain(req, res, next) {
    if(req.user == null || req.user.id == null || req.user.scope != "boat_captain") {
      res.status(401).json({
        message: unableToAuthMessage
      });
      return;
    }
    next();
}

/* verify angler scope */
function verifyBoatCaptainLimited(req, res, next) {
    if(req.user == null || req.user.id == null 
      || (req.user.scope != "boat_captain_limited" && req.user.scope != "boatcaptain_limited")) {
      res.status(401).json({
        message: unableToAuthMessage
      });
      return;
    }
    next();
}

/* verify fan parent scope */
function verifyFanParent(req, res, next) {
    if(req.user == null || req.user.id == null || req.user.scope != "fan_parent") {
      res.status(401).json({
        message: unableToAuthMessage
      });
      return;
    }
    next();
}

/* verify fan parent limited scope */
function verifyFanParentLimited(req, res, next) {
  if(req.user == null || req.user.id == null || (req.user.scope != "fanparent_limited" && req.user.scope != "fan_parent_limited")) {
    res.status(401).json({
      message: unableToAuthMessage
    });
    return;
  }
  next();
}

/* verify observer scope */
function verifyObserver(req, res, next) {
    if(req.user == null || req.user.id == null || req.user.scope != "observer") {
      res.status(401).json({
        message: unableToAuthMessage
      });
      return;
    }
    next();
}

/* verify observer limited scope */
function verifyObserverLimited(req, res, next) {
  if(req.user == null || req.user.id == null || req.user.scope != "observer_limited") {
      res.status(401).json({
        message: unableToAuthMessage
      });
      return;
    }
  next();
}

/* verify observer scope */
function verifyHighschoolAdmin(req, res, next) {
  if(req.user == null || req.user.id == null || req.user.scope != "highschool_admin") {
    res.status(401).json({
      message: unableToAuthMessage
    });
    return;
  }
  next();
}

/* WARNING: Requires State */
function verifyAcceptedHighschoolAdmin(req, res, next) {
  if(req.user == null || req.user.id == null || req.user.scope != "highschool_admin") {
    res.status(401).json({
      message: unableToAuthMessage
    });
    return;
  }

  /* check adminStatus */
  require('../../collections/highschoolAdminCollection').getById(req.user.id)
  .then(user => {
      if (user.adminStatus != null && user.adminStatus == "active_highschool_admin") { next(); return; }
      res.status(401).json({
        message: unableToAuthMessage
      });
   })
  .catch(error => { 
    res.status(401).json({
      message: unableToAuthMessage
    });
  })
}

function requireAnglerOrHighschoolAdmin(req, res, next) {
  if (req.user.scope != "angler" && req.user.scope != "highschool_admin") {
    res.status(401).json({
      message: unableToAuthMessage
    });
    return;
  }

  if (req.user.scope == "highschool_admin") {
    verifyAcceptedHighschoolAdmin(req, res, next);
    return;
  }

  if(req.user.scope == "angler") {
    verifyAngler(req, res, next);
  }
}

/* verify observer limited scope */
function verifyHighschoolAdminLimited(req, res, next) {
  if(req.user == null || req.user.id == null || (req.user.scope != "highschool_admin_limited" && req.user.scope != "highschool-admin_limited")) {
      res.status(401).json({
        message: unableToAuthMessage
      });
      return;
    }
  next();
}

module.exports = { 
    verifyBetaParticipant: verifyBetaParticipant,
    verifyAngler: verifyAngler,
    verifyAnglerLimited: verifyAnglerLimited,
    verifyBoatCaptain: verifyBoatCaptain,
    verifyBoatCaptainLimited: verifyBoatCaptainLimited,
    verifyFanParent: verifyFanParent,
    verifyFanParentLimited: verifyFanParentLimited,
    verifyObserver: verifyObserver,
    verifyObserverLimited: verifyObserverLimited,
    verifyHighschoolAdmin: verifyHighschoolAdmin,
    verifyAcceptedHighschoolAdmin: verifyAcceptedHighschoolAdmin,
    verifyHighschoolAdminLimited: verifyHighschoolAdminLimited,
    requireAnglerOrHighschoolAdmin: requireAnglerOrHighschoolAdmin
};