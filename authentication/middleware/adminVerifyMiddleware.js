const logger = require('../../common/logger');

var requireSuperAdmin = function requireSuperAdmin(req, res, next) {
    /* read from roles JSON */
    const fs = require('fs');
    let permissions = ["R", "W", "D"];
    let rawdata = fs.readFileSync('access/permissions.json');
    let adminPermissions = JSON.parse(rawdata);

    if (req.user == null || req.user.email == null || filter(adminPermissions, { email: req.user.email }).length <= 0) {
         logger.info(`--- Super Admin Authentication Failed ---`)
         res.status(401).json({
             message: "Unauthorized. Please authorize and try again."
         });
         return;
     }

    for(var i = 0; i < permissions.length; i++) {
        for(var j = 0; j < adminPermissions.length; j ++) {
            if (adminPermissions[j].email === req.user.email) {
                /* check if permissions contains read */
                if (adminPermissions[j].permissions.indexOf(permissions[i]) == -1) { 
                    logger.info(`--- Admin Authentication Failed... no Super Admin Permissions ---`)
                     res.status(401).json({
                         message: "Unauthorized. Please authorize and try again."
                     });
                     return;
                 }
            }
        }
    }
    logger.info(`--- Super Admin Authenticated ---`)
    next();
};

var requireReadAdmin = function requireReadAdmin(req, res, next) {
 /* read from roles JSON */
 const fs = require('fs');
 let permissions = ["R"];
 let rawdata = fs.readFileSync('access/permissions.json');
 let adminPermissions = JSON.parse(rawdata);

 if (req.user == null || req.user.email == null || filter(adminPermissions, { email: req.user.email }).length <= 0) {
    logger.info(`--- Read Admin Authentication Failed ---`)
     res.status(401).json({
         message: "Unauthorized. Please authorize and try again."
     });
     return;
 }

 for(var i = 0; i < permissions.length; i++) {
     for(var j = 0; j < adminPermissions.length; j ++) {
         if (adminPermissions[j].email === req.user.email) {
             /* check if permissions contains read */
             if (adminPermissions[j].permissions.indexOf(permissions[i]) == -1) { 
                logger.info(`--- Admin Authentication Failed... no Read Permission ---`)
                  res.status(401).json({
                      message: "Unauthorized. Please authorize and try again."
                  });
                  return;
              }
         }
     }
 }
 logger.info(`--- Read Admin Authenticated ---`)
 next();
};

var requireAdmin = function requireAdmin(req, res, next) {
    /* read from roles JSON */
    const fs = require('fs');
    let rawdata = fs.readFileSync('access/permissions.json');
    let adminPermissions = JSON.parse(rawdata);
   
    if (req.user == null || req.user.email == null || filter(adminPermissions, { email: req.user.email }).length <= 0) {
        logger.info(`--- Admin Authentication Failed ---`)
        res.status(401).json({
            message: "Unauthorized. Please authorize and try again."
        });
        return;
    }
   
    for(var j = 0; j < adminPermissions.length; j ++) {
        if (adminPermissions[j].email === req.user.email) {
            /* check if permissions contains read */
            logger.info(`--- Admin Authenticated ---`)
            next();
            return;
        }
    }
    logger.info(`--- Admin Authentication Failed ---`);
    res.status(401).json({
        message: "Unauthorized. Please authorize and try again."
    });
};

var requireWriteAdmin = function requireWriteAdmin(req, res, next) {
 /* read from roles JSON */
 const fs = require('fs');
 let permissions = ["W"];
 let rawdata = fs.readFileSync('access/permissions.json');
 let adminPermissions = JSON.parse(rawdata);

 if (req.user == null || req.user.email == null || filter(adminPermissions, { email: req.user.email }).length <= 0) {
    logger.info(`--- Write Admin Authentication Failed ---`)
     res.status(401).json({
         message: "Unauthorized. Please authorize and try again."
     });
     return;
 }

 for(var i = 0; i < permissions.length; i++) {
     for(var j = 0; j < adminPermissions.length; j ++) {
         if (adminPermissions[j].email === req.user.email) {
             /* check if permissions contains read */
             if (adminPermissions[j].permissions.indexOf(permissions[i]) == -1) { 
                logger.info(`--- Admin Authentication Failed... no Write Permission ---`)
                res.status(401).json({
                    message: "Unauthorized. Please authorize and try again."
                });
                  return;
              }
         }
     }
 }
 logger.info(`--- Write Admin Authenticated ---`)
 next();
};

function filter(arr, criteria) {
    return arr.filter(function(obj) {
      return Object.keys(criteria).every(function(c) {
        return obj[c] == criteria[c];
      });
    });
}

module.exports = { 
    requireAdmin: requireAdmin,
    requireReadAdmin: requireReadAdmin,
    requireWriteAdmin: requireWriteAdmin,
    requireSuperAdmin: requireSuperAdmin
}