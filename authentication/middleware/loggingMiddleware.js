const logger = require('../../common/logger');
/* verify observer scope */
function log(req, res, next) {
    if (process.env.NODE_ENV == "test") { logger.info(req.originalUrl); }
    next();
}

module.exports = log 