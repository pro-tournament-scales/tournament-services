const unableToAuthMessage = "We were unable to authtenticate you. Please try again later."
const logger = require('../../common/logger')
function handleBasicAuth(req, res, next) {
    if (req.headers.authorization == null) { 
        res.status(401).json({
            message: unableToAuthMessage
        });
        return; 
    }

    let encodedAuthCode = req.headers.authorization.replace('Basic ', '')
    let buff = new Buffer.from(encodedAuthCode, 'base64'); 
    let authCode = buff.toString('ascii');
    let authSplit = authCode.split(':');

    if (authSplit.length <= 1) {
        res.status(401).json({
            message: unableToAuthMessage
        });
        return; 
    }

    req.headers.email = authSplit[0];
    req.headers.password = authSplit[1];
    next();
}

/* webhook to handle document status updates from eSignatures */
function eSignatureBasicAuth(req, res, next) {
    logger.debug("a request requiring e signature basic auth has come through");
    if (req.headers.authorization == null) { 
        logger.warn("No authorization token found for e signature basic auth");
        res.status(401).json({
            message: unableToAuthMessage
        });
        return; 
    }

    let encodedAuthCode = req.headers.authorization.replace('Basic ', '')
    let buff = new Buffer.from(encodedAuthCode, 'base64'); 
    let authCode = buff.toString('ascii');
    let authSplit = authCode.split(':');

    if (authSplit.length < 1) {
        logger.warn("e signature missing token");
        res.status(401).json({
            message: unableToAuthMessage
        });
        return; 
    }

    const secrectToken = authSplit[0];
    if (process.env.E_SIGNATURE_SECRET_TOKEN != secrectToken) {
        logger.warn("e signature token mismatch");
        res.status(401).json({
            message: unableToAuthMessage
        });
        return; 
    }
    next();
}

module.exports = { 
    handleBasicAuth: handleBasicAuth,
    eSignatureBasicAuth: eSignatureBasicAuth
}