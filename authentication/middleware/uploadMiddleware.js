const Multer = require('multer');
const multiparty = require('multiparty');

const multer = Multer({
  storage: Multer.memoryStorage(),
  limits: {
    fileSize: 20 * 1024 * 1024, // no larger than 20mb, you can change as needed.
  },
});

var parseForm = function parseForm(req, resp, next) {
    try {
        var form = new multiparty.Form();
        form.parse(req, function(err, fields, files) {
          var body = {};
          if (fields == null) { resp.status(400, { message: "No fields were passed. Please try again."}); return; }
           Object.keys(fields).forEach(function(name) {
            if(fields[name][0] != null && fields[name][0] != undefined) {
                body[name] = fields[name][0];
            }
          });
    
         Object.keys(files).forEach(function(name) {
                if(files[name][0] != null && files[name][0] != undefined) {
                   body[name] = files[name][0];
                }
          });
          req.body = body;
          next();
        });
    } catch(err) {
        // err
        console.log(`-- error in upload middleware ${err} --`);
         resp.status(401).json({
            message: "Unauthorized. Please sign in."
        });
    }
};

module.exports = { multer: multer, parseForm: parseForm };