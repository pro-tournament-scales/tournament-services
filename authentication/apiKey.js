module.exports = apiKey;
const logger = require('../common/logger');
const unableToAuthMessage = "We were unable to authtenticate you. Please try again later."
const apiKeyObj = require("./apiKeyConfig.json");
const ignore = [
    "/v1/document/status",
    /\/v1\/angler\/verify\/\w*/,
    /\/v1\/boatcaptain\/verify\/\w*/,
    /\/v1\/fanparent\/verify\/\w*/,
    /\/v1\/observer\/verify\/\w*/,
    /\/v1\/highschool-admin\/verify\/\w*/,
]
function apiKey(req, res, next) {
    var ignorable = false;
    ignore.forEach(element => {
        if (req.path.match(element)) { ignorable = true; } 
    });

    if (ignorable) { next(); return; }

    if(req.headers == null || req.headers.apikey == null) {
        return res.status(401).json({ message: unableToAuthMessage });
    }
    
    let buff = new Buffer.from(req.headers.apikey, 'base64'); 
    let text = buff.toString('ascii');
    if(apiKeyObj.decodedKey.localeCompare(text) > 0) {
        return res.status(401).json({ message: unableToAuthMessage });
    }
    
    next();
}