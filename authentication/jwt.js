const expressJwt = require('express-jwt');
const jwtsecret = process.env.JWT_SECRET || 'THIS-IS-A-TEST-SECRET-KEY-PLEASE-REPLACE';
const adminjwtsecret = process.env.ADMIN_JWT_SECRET || 'THIS-IS-A-TEST-SECRET-KEY-FOR-ADMIN-PLEASE-REPLACE';

module.exports = {
    user: user,
    admin: admin
};

function user() {
    return expressJwt({ secret: jwtsecret, algorithms: ['HS256'] }).unless({
        path: [
            // general public routes
            '/v1/contact/enquire',
            // boat captain public routes
            '/v1/boatcaptain/new',
            '/v1/boatcaptain/login',
            '/v1/boatcaptain/forgot/password',
            /\/v1\/boatcaptain\/verify\/\w*/,
            /\/v1\/boatcaptain\/exists\/email\/\w*/,
            // angler public routes
            '/v1/angler/new',
            '/v1/angler/login',
            '/v1/angler/forgot/password',
            /\/v1\/angler\/verify\/\w*/,
            /\/v1\/angler\/exists\/email\/\w*/,
            // fan parent public routes
            '/v1/fanparent/new',
            '/v1/fanparent/login',
            '/v1/fanparent/forgot/password',
            /\/v1\/fanparent\/verify\/\w*/,
            /\/v1\/fanparent\/exists\/email\/\w*/,
            // observer public routes
            '/v1/observer/new',
            '/v1/observer/login',
            '/v1/observer/forgot/password',
            /\/v1\/observer\/verify\/\w*/,
            /\/v1\/observer\/exists\/email\/\w*/,
            // highschool admin routes
            '/v1/highschool-admin/new',
            '/v1/highschool-admin/login',
            '/v1/highschool-admin/forgot/password',
            /\/v1\/highschool-admin\/verify\/\w*/,
            /\/v1\/highschool-admin\/exists\/email\/\w*/,
        ]
    });
}

function admin() {
    return expressJwt({ secret: adminjwtsecret, algorithms: ['HS256'] }).unless({
        path: [
            //admin public routes
        ]
    });
}