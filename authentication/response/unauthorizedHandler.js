module.exports = errorHandler;

const unableToAuthMessage = "We were unable to authtenticate you. Please try again later."

function errorHandler(err, req, res, next) {
    if (typeof (err) === 'string') {
        // custom application error
        return res.status(400).json({ message: err });
    }

    if (err.name === 'UnauthorizedError' 
    || err.name === 'TokenExpiredError'
    || err.name === 'JsonWebTokenError'
    || err.name === 'NotBeforeError') {
        // jwt authentication error
        return res.status(401).json({ message: unableToAuthMessage });
    }

    // default to 500 server error
    return res.status(500).json({ message: err.message });
}