const logger = require('./logger');
function batch(
  values,
  promise = (value) => new Promise((_, reject) => { reject("Unimplemented"); }),
  options = {
     batchSize: 20,
     timeoutMs: 3600000
}) {

    let results = [];
    let errors = [];
    let timeoutHandle;

    let batchSize = options.batchSize;
    let timeoutMs = options.timeoutMs;

    let batchPromise = new Promise((resolve, reject) => {

      let batches = values.chunk_inefficient(batchSize);

      if (batches.length == 0) { resolve(results); return; }
      var chain = Promise.all(batches[0].map((value) => promise(value), errors))
      .then(result => new Promise((resolve, _) => { results.push(result); resolve(result); }));

      for (var i = 1; i < batches.length; i++) {
          let batch = batches[i];
          chain = chain.then(() => Promise.all(batch.map((value) => promise(value), errors)))
          .then(result => new Promise((resolve, _) => { results.push(result); resolve(result); }))
      }

      chain = chain.then(() => {
          if (flattenResults(results)) { 
            var flattenedResults = flattenResults(results);
            flattenedResults = flattenedResults.filter((result) => result !== undefined);
            resolve(flattenedResults); 
          }
          if (errors.length > 0) { reject(errors); }          
      });
      chain.catch(error => new Promise((resolve, _) => { errors.push(error); resolve(); }));;
  });

  const timeoutPromise = new Promise((resolve, reject) => {
    timeoutHandle = setTimeout(() => {
      logger.warn("batch timed out before process could complete");
      var flattenedResults = flattenResults(results);
      flattenedResults = flattenedResults.filter((result) => result !== undefined);
      resolve(flattenedResults);
      if (errors.length > 0) { reject(errors); } 
    }, timeoutMs);
  });

  return Promise.race([batchPromise, timeoutPromise])
  .then((result) => {
    clearTimeout(timeoutHandle);
    return result;
  });
}

function flattenResults(results) {
  return results.reduce(
    function(a, b) {
      return a.concat(b);
    },
    []
  );
}

Object.defineProperty(Array.prototype, 'chunk_inefficient', {
    value: function(chunkSize) {
      var array = this;
      return [].concat.apply([],
        array.map(function(elem, i) {
          return i % chunkSize ? [] : [array.slice(i, i + chunkSize)];
        })
      );
    }
  });

  module.exports = {
    batch: batch
  }