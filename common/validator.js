/* validate latitude is formatted correctly */
var isLatitudeFormattedCorrectly = function(latitude) {
    if( /^(\+|-)?(?:90(?:(?:\.0{1,16})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,6})?))$/.test(latitude) ) {
        return true;
    }
    return false;
};

/* validate longitude is formatted correctly */
var isLongitudeFormattedCorrectly = function(longitude) {
    if( /^(\+|-)?(?:180(?:(?:\.0{1,16})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/.test(longitude) ) {
        return true;
    }
    return false;
};

/* validate email format */
function isEmailValid(email) {
    if (email == null) { return false; }
    return email.trim().match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/) != null;
}

/* validate street address */
function isAddressValid(address) {
    if (address == null) { return false; }
    return address.trim().match(/^\s*\S+(?:\s+\S+){2}/) != null;
}

/* validate date of birth */
function isDateValid(dob) {
    if (dob == null) { return false; }
    return dob.trim().match(/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/) != null;
}

/* validate that school grade is valid */
function isGradeValid(grade) {
    if (grade == null) { return false; }
    return grade >= 9 && grade <= 12;
}

/* validate that password meets requirements */
function isPasswordValid(password) {
    if (password == null) { return false; }
    return password.trim().length >= 6;
}

/* validate that a person's name is valid */
function isPersonNameValid(name) {
    if (name == null) { return false; }
    return name.trim().match(/^[a-zA-Z ]+$/) != null && name.length < 128;
}

function isPhoneNumberValid(number) {
    if (number == null) { return false; }
    return number.trim().match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/) != null
}

// TODO: actually validate make, model, year, horsePower
function isBoatDetailsValid(boatDetails) {
    if (boatDetails == null || boatDetails.make == null || boatDetails.model == null
        || boatDetails.year == null|| boatDetails.horsepower == null) { return false; }
    /* TODO: Check for TX number */    
    return true;
    // return (boatDetails.make.match(/^[a-zA-Z ]+$/) 
    // && boatDetails.model.match(/^[a-zA-Z ]+$/)
    // && boatDetails.year.match(/^(19[5-9]\d|20[0-4]\d|2050)$/))
    // && boatDetails.horsepower.match(/^\d{4}$/)
}

module.exports = { 
    isLongitudeFormattedCorrectly: isLongitudeFormattedCorrectly,
    isLatitudeFormattedCorrectly: isLatitudeFormattedCorrectly,
    isEmailValid: isEmailValid,
    isAddressValid: isAddressValid,
    isDateValid: isDateValid,
    isGradeValid: isGradeValid,
    isPasswordValid: isPasswordValid,
    isPersonNameValid: isPersonNameValid,
    isPhoneNumberValid: isPhoneNumberValid,
    isBoatDetailsValid: isBoatDetailsValid
};