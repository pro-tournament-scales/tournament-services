var accountSid = process.env.TWILIO_ACCOUNT_SID; // Your Account SID from www.twilio.com/console
var authToken = process.env.TWILIO_AUTH_TOKEN;   // Your Auth Token from www.twilio.com/console
const fromNumber = process.env.TWILIO_PHONE_NUMBER;

const client = require('twilio')(accountSid, authToken, { 
    lazyLoading: true 
});

function sendMessage(to, message) {
    return client.messages.create({ body: message, to: to, from: fromNumber });
}

module.exports = { sendMessage: sendMessage }