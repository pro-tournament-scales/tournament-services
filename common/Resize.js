const sharp = require('sharp');
const { uuid } = require('uuidv4');
const path = require('path');
var filepath = ""

class Resize {
  constructor(folder) {
    filepath = folder;
  }
  
  save(tmpfilename, tmpfilepath, baseUrlPath, key) {
    return new Promise((res, rej) => {
      const fs = require('fs');
      fs.readFile(tmpfilepath, function read(err, data) {
        if(err) {
          console.log("ERROR: " + err);
          rej({ message: "Unable to upload image. Please verify that the image is of type PNG."});
        }
        
        if(data == null || data.buffer == null) {
          rej({ message: "Unable to upload image. Please verify that the image is of type PNG."});
        }
        const filename = Resize.filename();
        const newfilepath = Resize.filepath(filename);
  
        sharp(new Buffer(data.buffer))
          .resize(640, 480, {
            fit: sharp.fit.inside,
            withoutEnlargement: true
          })
          .toFile(newfilepath);
        res({ filepath: baseUrlPath + "/" + filename, key: key });
      });
    });
  }

  getDataBuffer(tmpfilepath, width, height) {
    return new Promise((res, rej) => {
      const fs = require('fs');
      fs.readFile(tmpfilepath, function read(err, data) {
        if(err) {
          console.log("ERROR: " + err);
          rej({ message: "Unable to upload image. Please verify that the image is of type PNG."});
        }
        
        if(data == null || data.buffer == null) {
          rej({ message: "Unable to upload image. Please verify that the image is of type PNG."});
        }

        sharp(new Buffer(data.buffer))
        .resize(width, height, {
            fit: sharp.fit.inside
        })
        .sharpen()
        .toBuffer()
        .then(function(outputBuffer) {
            res(outputBuffer);
        })
        .catch(function(error) {
            rej(error);
        });
      });
    });
  }
  
  static filename() {
    return `${uuid()}.png`;
  }
  
  static filepath(filename) {
    return path.resolve(filepath + "/" + filename);
  }
}
module.exports = Resize;