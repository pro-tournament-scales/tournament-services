function flatten(arr) {
    return arr.reduce(function (flat, toFlatten) {
      return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten);
    }, []);
}

function trim(obj) {
  Object.keys(obj).map(k => obj[k] = typeof obj[k] == 'string' ? obj[k].trim() : obj[k]);
}

function sum(arr, key) {
  return arr.reduce((a, b) => (a || 0) + (b[key] || 0), 0);
}

function groupBy(collection, property) {
  var i = 0, val, index,
      values = [], result = [];
  for (; i < collection.length; i++) {
      val = collection[i][property];
      index = values.indexOf(val);
      if (index > -1)
          result[index].push(collection[i]);
      else {
          values.push(val);
          result.push([collection[i]]);
      }
  }
  return result;
}

function promiseWithKey(promise, key) {
  return new Promise((resolve, reject) => {
    promise
    .then(data => resolve({ key: key, data: data }))
    .catch(error => reject({ key: key, error: error }))
  });
}

/* date operation to calculate time */
function calcTime(offset, d = new Date()) {
  // convert to msec
  // subtract local time zone offset
  // get UTC time in msec
  var utc = d.getTime() + (d.getTimezoneOffset() * 60000);

  // create new Date object for different city
  // using supplied offset
  var nd = new Date(utc + (3600000*offset));

  // return time as a string
  return nd;
}

module.exports = { flatten: flatten, trim: trim, groupBy: groupBy, promiseWithKey: promiseWithKey, calcTime: calcTime, sum: sum }