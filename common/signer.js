const secretToken = process.env.E_SIGNATURE_SECRET_TOKEN
const request = require('request');
const logger = require('./logger');

const anglerDocument = "26761c6a-1ab9-4661-a6e3-32c8562813ec";
const boatCaptainDocument = "0b4406ed-4743-4954-bca0-0641d42387f9";
const highschoolAdminDocument = "03177363-b815-4ef2-a7a1-768e13e33c2b";
const observerDocument = "6668c192-e524-44f3-96b8-8a5804f497a4";

function signAndSendDocument(templateId, userType, userId, signers) {
    return new Promise(function(resolve, reject) { 

        if (templateId == null 
            || userType == null 
            || userId == null 
            || (userType != "angler" && userType != "boatcaptain" && userType != "fanparent" && userType != "observer" && userType != "highschool-admin")
            || signers == null) {
            logger.debug("Missing required information. Could not send documents.");
            reject("Missing required information. Could not send documents.");
            return; 
        }
    
        const metadata = JSON.stringify({
            userType: userType,
            userId: userId
        });

        signers.forEach(signer => {
           if (signer.name == null 
            || signer.email == null
            || signer.signing_order == null) {
               logger.debug("Missing required information. Could not send documents.");
               reject("Missing required information. Could not send documents.");
               return;
           }
        });

        var options = {
            url: `https://${secretToken}:@esignatures.io/api/contracts`,
            headers: { 
                'Content-Type': `application/json`
                },
            body: {
                template_id: templateId,
                signers: signers,
                metadata: metadata
            },
            json: true
        };

        /* handle callback result */
        function callback(error, response, body) {
            if (!error && (response.statusCode == 200) && body != null) {
                logger.debug("documents have been sent for signing");
                resolve(body)
            } else { 
                logger.warn(error);
                logger.warn(`eSignatures failed with response ${JSON.stringify(response)}`);
                reject(error || "Something went wrong when sending the document. Please try again later.");
            }
        }

        /* make internal request, set auth headers */
        request.post(options, callback);
    });
}

module.exports = { 
    anglerDocument: anglerDocument,
    boatCaptainDocument: boatCaptainDocument,
    highschoolAdminDocument: highschoolAdminDocument,
    observerDocument: observerDocument,
    signAndSendDocument: signAndSendDocument 
}