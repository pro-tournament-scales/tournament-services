const generalMsg = "Something went wrong. Please try again later.";
const noUserFoundMsg = "Failed to retrieve user profile. Please try again later.";
const missingRequiredInfoMsg = "Missing required information. Please try again.";
const noMatchingUserMsg = "No matching user was found. Please try again later.";
const failedToSendEmail = "Failed to send email. Please try again later.";
const badPassword = "Invalid password. Please make sure to include at least one letter, number, and special character.";
const noMatchingPassword = "The old password you entered does not match the correct password. Please try again.";
const failedToUploadProfileIcon = "Failed to upload profile icon. Please try again later";
const failedToUploadSafeSport = "Failed to upload safe sport document. Please try again later";
const failedToDeleteSafeSport = "Failed to delete safe sport document. Please try again later";
const invalidEmailOrPasswordMsg = "Invalid email or password. Please try again.";
const userExistsForEmail = "A user of this account type and email already exists. Please try a different email.";
const nameFormatMsg = "Your first or last name is in an invalid format. Please try again.";
const missingEmailMsg = "Missing or invalid email. Please try again.";
const missingPasswordMsg = "Please make sure the password you set is at least 6 characters long. For example: password123";

const uploader = require('./uploader');
const mailer = require('./mailer');
const logger = require('./logger');
const operations = require('./operations');
const pushnotifier = require('./pushnotifier');
const bcrypt = require("bcrypt");
const validator = require('./validator');
const verifier = require('./verifier');
const jwt = require('jsonwebtoken');
const UserCollection = require('./userCollection');
const db = require("../common/dbConnection").db;
const jwtsecret = process.env.JWT_SECRET || 'THIS-IS-A-TEST-SECRET-KEY-PLEASE-REPLACE';
const domainHost = process.env.HOST;

function successResponse(status = 200, body = {}) {
    return {
        status: status,
        body: body
    }
}

function errorResponse(status = 400, message = "Something went wrong. Please try again later.") {
    return {
        status: status,
        message: message
    }
} 

function encryptPassword(password) {
    return new Promise(function(resolve, reject) {
        password = password.trim();
        encrypedPassword = bcrypt.hashSync(password, 10);
        resolve(successResponse(200, encrypedPassword))
    });
}

function authenticate(email, password) {
    return new Promise(function(resolve, reject) {
        console.log("attempting to auth");

        new UserController(new UserCollection(db.users), "beta", "beta").getByEmail(email)
        .then(function(response) {
            let user = response.body
            console.log("user - " + JSON.stringify(user))
            console.log("attempting to create token");

            if (user == null) { reject(errorResponse(401, noUserFoundMsg)); return; }
            console.log("attempting to create token2 - " + password + ", " + user.password);
            if(!(bcrypt.compareSync(password, user.password))) { 
                
                console.log("passwords dont match!");
                reject(errorResponse(401, invalidEmailOrPasswordMsg)); return; } 
            // create token
            console.log("creating token!");
            
            let options = { expiresIn: "60d" };
            let scope = 'beta'
            let token = jwt.sign({ id: user._id, email: user.email, scope: scope}, jwtsecret, options);
            console.log("token = " + token)

            resolve(successResponse(200, {
                access_token: token,
                token_type: "bearer",
                expires_in: "60d",
                scope: scope
            }))
        })
        .catch(function(error) { reject(errorResponse(500, error)); });
    });
}


/**
 * 
 * @param {*} collection actual mongo collection API
 * @param {*} scope token scope used in JWT
 * @param {*} root root of public APIs
 * @param {*} verify function used to additionally verify data is proper
 */
function UserController(collection, scope, root = scope, verify = (user) => { return new Promise(function(resolve, reject) { return resolve(user) }); }) { 

    this.createUser = function(data) {
        return new Promise(function(resolve, reject) {
            if (data == null) { reject(errorResponse(400, missingRequiredInfoMsg)); return; }
            // First
            if (data.firstName == null || !validator.isPersonNameValid(data.firstName)) { reject(errorResponse(400, nameFormatMsg)); return; }
            // Last
            if (data.lastName == null || !validator.isPersonNameValid(data.lastName)) { reject(errorResponse(400, nameFormatMsg)); return; }
            // Email
            if (data.email == null || !validator.isEmailValid(data.email)) { reject(errorResponse(400, missingEmailMsg)); return; }
            // Password
            if (data.password == null || !validator.isPasswordValid(data.password)) { reject(errorResponse(400, missingPasswordMsg)); return; }

            verify(data)
            .then(user => {
                operations.trim(user);
                // set default account status to none
                user.status = "none";
                // hash password 
                user.password = bcrypt.hashSync(user.password, 10);
                // set created timestamp
                user.created = new Date();

                collection.getByEmail(user.email).then(function(existingUser) {
                    /* resolve to outer promise */
                    if (existingUser != null) { reject(errorResponse(429, userExistsForEmail)); return; }
                    /* create user */
                    collection.createUser(user)
                    .then(function(response) { 
                        require('../controllers/subscriptionController').subscribe("global", response._id.toString());
                        verifier.sendUserVerify(response._id, response.email, `${scope}_limited`, root);
                        delete response.password; // remove password from response
                        resolve(successResponse(200, response)); 
                    })
                    .catch(function(error) { 
                        logger.warn(`something went wrong while creating ${scope} account ${error}`); 
                        reject(errorResponse(500, generalMsg)); 
                    });
                })

            })
            .catch(error => reject(error));
        });
    }

    this.getById = function(id, deletePass = true, squashUser = false) {
        return new Promise(function(resolve, reject) {
            if (id == null) { reject(errorResponse(400, missingRequiredInfoMsg)); return; }
            collection.getById(id, squashUser)
            .then(function(user) { 
                if (user == null) { 
                    logger.debug(`no user found for id: ${id} for collection ${collection}`, id, "user");
                    reject(errorResponse(404, `Failed to retrieve profile for account type: ${scope}. Please try again later.`)); 
                    return; 
                }
                if (deletePass && user.password != null) { delete user.password; }
                resolve(successResponse(200, user));
            })
            .catch(function(error) { reject(errorResponse(500, generalMsg)); })
        });
    }

    this.getUsersById = function(ids, deletePass = true, squashUser = false) {
        return new Promise(function(resolve, reject) {
            if (ids == null || ids.length == 0) { resolve(successResponse(200, [])); return; }
            collection.getUsersById(ids, squashUser)
            .then(function(users) { 
                if (users == null) { 
                    logger.debug(`no users found for ids: ${ids} for collection ${collection}`);
                    reject(errorResponse(404, `Failed to retrieve users for account type: ${scope}. Please try again later.`)); 
                    return; 
                }
                users.forEach(user => {
                    if (deletePass && user.password != null) { delete user.password; }
                })
               
                resolve(successResponse(200, users));
            })
            .catch(function(error) { reject(errorResponse(500, generalMsg)); })
        });
    }

    this.getByEmail = function(email) {
        console.log("getting user - " + email);
        return new Promise(function(resolve, reject) {
            if (email == null) { reject(errorResponse()); return; }
            if (!validator.isEmailValid(email)) { reject(errorResponse()); return; }
            console.log("passed validations");
            collection.getByEmail(email)
            .then(function(user) { 
                console.log("found user - " + JSON.stringify(user));
                resolve(successResponse(200, user)); })
            .catch(function(error) { reject(errorResponse(500, "Something went wrong. Please try again later.")); });
        })
    }

    this.getElligibleUsers = function(limited = true, eligible = function(user) { return true; }) {
        return new Promise(function(resolve, reject) {
            var squashUser = false;
            if (limited == true || limited == "true") { squashUser = true; }
            collection.getUsers(squashUser)
            .then(function(users) { 
                /* remove detailed information if limited */
                users = users.filter(function(user) {
                    return user.status == "verified" 
                    && eligible(user);
                })

                resolve(successResponse(200, users));
            })
            .catch(function(error) { reject(errorResponse()); })
        });
    }

    this.searchElligibleUsers = function(firstName, lastName, limited = true, eligible = function(user) { return true; }) {
        return new Promise(function(resolve, reject) {
            if (firstName == null && lastName == null) { resolve(successResponse(200, [])); return; }
            var squashUser = false;
            if (limited == true || limited == "true") { squashUser = true; }
            collection.searchUsers(firstName, lastName, squashUser)
            .then(function(users) { 
                /* remove detailed information if limited */
                users = users.filter(function(user) {
                    return user.status == "verified" 
                    && eligible(user);
                })

                resolve(successResponse(200, users));
            })
            .catch(function(error) { reject(errorResponse()); })
        });
    }

    this.userExistsForEmail = function(email) {
        return new Promise(function(resolve, reject) {
                        if (email == null) { reject(errorResponse()); return; }
            if (!validator.isEmailValid(email)) { reject(errorResponse()); return; }
            collection.getByEmail(email)
            .then(function(user) { resolve(successResponse(200, { exists: user != null })); })
            .catch(function(error) { reject(errorResponse(500, "Something went wrong. Please try again later.")); });
        })
    }

    
    this.verify = function(id) {
        return new Promise(function(resolve, reject) {
            collection.getById(id)
            .then(user => {
                if (user != null && user.status != null && user.status == "verified") {
                    logger.info("user already verified. No need to re-verify", id, "user");
                    return;
                }

                /* update account with verified status */
                collection.updateVerified(id)
                .then(function(user) { 
                    pushnotifier.broadcastMessage(id, "Account Verified 🎉", "Your account has been verified. For Anglers, Boat Captains, and Highschool Admins, and Observers we have emailed you some additional documents that you need to sign.");
                    resolve(successResponse(200, user));
                })
                .catch(function(error) { reject(errorResponse(500, "Something went wrong. Please try again later.")); });
            })
            .catch(error => { reject(errorResponse(500, "Something went wrong. Please try again later.")); })
        })
    }



    /* handle password forgotten */
    this.forgotPassword = function(email, type) {
        return new Promise(function(resolve, reject) { 
            logger.info(`A user has forgotten password and requested password reset for email ${email} for type ${type}`);
            collection.getByEmail(email).then(function(user) {
                if (user == null) { reject(errorResponse(404, noMatchingUserMsg)); return; }
                if (user.email == null) { reject(errorResponse(400, missingRequiredInfoMsg)); return; } 
                /* generate jwt token with given type */
                let options = { expiresIn: "3d" };
                let token = jwt.sign({ id: user._id, email: user.email, verified: user.status == "verified", scope: `${type}_limited` }, jwtsecret, options);
                const rawUrl = `${domainHost}/${type}/reset.html?token=${token}`;
                /*  */
                mailer.formatLinkForEmail(rawUrl).then(function(verifyUrl) {
                    mailer.send(email, "PTS - Forgot Password", `You have notified us about a forgotten password. To reset, navigate to the following url: ${verifyUrl}`)
                    .then(function(response) { console.log(`-- reset password email has been sent ${response} --`); resolve(successResponse()); })
                    .catch(function(error) { console.log(`-- failed to send email due to error ${error} --`); reject(errorResponse()); });
                })
                .catch(function(error) { reject(errorResponse(500, failedToSendEmail)); });
            })
            .catch(function(error) { reject(errorResponse(500, generalMsg)); })
        })
    }

    /* when a user simply wants to change their current password */
    this.changePassword = function(id, oldPassword, newPassword) { 
        return new Promise(function(resolve, reject) { 
            if (id == null || oldPassword == null || newPassword == null) { reject(errorResponse(400, missingRequiredInfoMsg)); return; }
            if (!validator.isPasswordValid(newPassword)) { reject(errorResponse(400, badPassword)); return; }
            /* verify old password match */
            collection.getById(id).then(function(user) { 
                if (user == null || user.password == null) { reject(errorResponse(500, generalMsg)); return; }
                /* compare passwords */
                if(!(bcrypt.compareSync(oldPassword, user.password))) { reject(errorResponse(401, noMatchingPassword)); return; } 
                password = password.trim();
                password = bcrypt.hashSync(newPassword, 10);
                collection.updatePassword(id, password)
                .then(function(user) {
                    delete user.password; 
                    resolve(successResponse(200, user));
                    logger.info(`user with id ${user._id} and email ${user.email} has successfully reset their password`);
                    mailer.send(user.email, "PTS - Password Changed", `Your password has been changed. Please contact support@PTS.net if this was not you.`)
                    .then(function(response) { logger.debug(`-- password changed email has been sent ${response} --`, id, "user"); })
                    .catch(function(error) { logger.warn(`-- failed to send password changed email due to error ${error} --`, id, "user"); });
                 })
                .catch(function(error) { reject(errorResponse(500, generalMsg)); })
            })
            .catch(function(error) { reject(errorResponse(500, generalMsg)); })
        })
    }

    /* when the system wants to update a users password. Mostly used for forgot password flow */
    this.updatePassword = function(id, password) {
        return new Promise(function(resolve, reject) {
            if (id == null || password == null) { reject(errorResponse(400, missingRequiredInfoMsg)); return; }
            if (!validator.isPasswordValid(password)) { reject(errorResponse(400, badPassword)); return; }
            password = password.trim();
            password = bcrypt.hashSync(password, 10);
            collection.updatePassword(id, password)
            .then(function(user) { 
                resolve(successResponse(200, user));
                logger.info(`user with id ${user._id} and email ${user.email} has successfully updated their password`);
                mailer.send(user.email, "PTS - Password Changed", `Your password has been changed. Please contact support@PTS.net if this was not you.`)
                .then(function(response) { logger.debug(`-- password changed email has been sent ${response} --`); })
                .catch(function(error) { logger.warn(`-- failed to send password changed email due to error ${error} --`); });
             })
            .catch(function(error) { reject(errorResponse(500, generalMsg)); })
        })
    }

    /* stores profile icon if one doesn't already exist. Updates icon if one already exists for user */
    this.updateProfileIcon = function(id, email, image) {
        return new Promise(function(resolve, reject) {
            uploader.uploadIconToStorage(email, "profile_icon", image)
            .then(function(result) {
                /* retrieve existing profile iconUrl and delete */
                collection.getById(id).then(function(response) {
                    if (response == null) { reject(errorResponse(500, failedToUploadProfileIcon)); return; }
                    uploader.removeImage(response.iconUrl); // remove existing image from storage if exists
                    collection.updateProfileIcon(id, result.url).then(function(response) {
                        resolve(successResponse(200, response));    
                    }).catch(function(error) {
                        logger.debug(`failed to update profile icon with url ${result.url}`, id, "user")
                        uploader.removeImage(result.url);
                        reject(errorResponse(500, failedToUploadProfileIcon));
                    })
                })
                .catch(function(error) {
                    logger.debug(`Icon failed to upload with error: ${JSON.stringify(error)}`, id, "user");
                    uploader.removeImage(result.url);
                    reject(errorResponse(500, failedToUploadProfileIcon));
                });
            })
            .catch(function(error) {
                logger.warn(`Icon failed to upload with error: ${JSON.stringify(error)}`, id, "user");
                reject(errorResponse(500, failedToUploadProfileIcon));
            })
        });
    }

    this.updateSafeSportUrl = function(id, email, doc) {
        return new Promise(function(resolve, reject) {
            uploader.uploadFileToStorage(email, "safe_sport", doc, "users")
            .then(result => {
                      /* retrieve existing profile iconUrl and delete */
                      collection.getById(id).then(function(response) {
                        if (response == null) { reject(errorResponse(500, failedToUploadSafeSport)); return; }
                        uploader.removeImage(response.safeSportUrl); // remove existing image from storage if exists
                        collection.updateSafeSportUrl(id, result.url).then(function(response) {
                            resolve(successResponse(200, response));    
                        }).catch(function(error) {
                            logger.debug(`failed to update profile icon with url ${result.url}`, id, "user")
                            uploader.removeImage(result.url);
                            reject(errorResponse(500, failedToUploadSafeSport));
                        })
                    })
                    .catch(function(error) {
                        logger.debug(`Safe Sport failed to upload with error: ${JSON.stringify(error)}`, id, "user");
                        uploader.removeImage(result.url);
                        reject(errorResponse(500, failedToUploadSafeSport));
                    });
            })
            .catch(error => {
                logger.warn(`Safe Sport failed to upload with error: ${JSON.stringify(error)}`, id, "user");
                reject(errorResponse(500, failedToUploadSafeSport));
            })
        });
    }

    this.removeAllSafeSportUrls = function() {
        collection.getUsers(false, {}).then((response) => {
            if (response == null || response.length == 0) { reject(errorResponse(500, failedToDeleteSafeSport)); return; }
            response.forEach(user => {
                if (user.safeSportUrl != null) {
                    uploader.removeImage(user.safeSportUrl); // remove existing image from storage if exists
                    collection.updateSafeSportUrl(user._id, null).then(function(response) {
                        logger.debug(`deleted safe sport url`);  
                    }).catch(function(error) {
                        logger.debug(`failed to delete safe sport with url ${user.safeSportUrl} with error ${error}`)
                    })
                }
            });
        })
    }

    this.updateDocumentStatus = function(id, status) {
        return new Promise(function(resolve, reject) {
            logger.debug("document status attempting to update", id, "user");
            if (status == null) {
                logger.debug("document status could not update due to missing id or status", id, "user");
                reject(errorResponse()); 
                return;
            }

            /* id is required to update to signed */
            if (status != "contract-signed") {
                logger.info(`document status updated to ${status}`, id, "user");
                resolve(successResponse()); 
                return;
            }

            if (id == null) { 
                logger.warn("id is null even tho contract-signed. Could not update account status");
                reject(errorResponse()); 
                return;
            }

            /* get user by id to verify documents not already completed */
            collection.getById(id)
            .then(user => {
                if (user != null && user.documentsCompleted != null && user.documentsCompleted == true) {
                    logger.info("did not update documents complete, user documents are already comleted", id, "user");
                    return;
                }
                
                /* documents have been signed */
                let documentStatusMessage = "All documents have been signed.";
                let documentsCompleted = true;
        
                collection.updateDocumentStatus(id, documentsCompleted, documentStatusMessage)
                .then(response => { 
                    pushnotifier.broadcastMessage(id, "Documents have been signed 📄", "All of the required documents have been signed and submitted. If you are an angler, you may now register for PTS annual membership.");
                    logger.debug("collection updated with document status"); 
                    resolve(successResponse()); 
                })
                .catch(error => { 
                    logger.debug("collection failed to update with document status");
                    reject(errorResponse()); 
                })
            })
            .catch(error => { reject(errorResponse(500, "Something went wrong. Please try again later.")); }) 
        });
    },

    this.updateUser = function(user) {
        return new Promise((resolve, reject) => {
            // First
            if (user.firstName == null || !validator.isPersonNameValid(user.firstName)) { reject(errorResponse(400, nameFormatMsg)); return; }
            // Last
            if (user.lastName == null || !validator.isPersonNameValid(user.lastName)) { reject(errorResponse(400, nameFormatMsg)); return; }
            // Verify additional data
            verify(user)
            .then(user => { 
                collection.updateUser(user)
                .then(updatedUser => resolve(successResponse(200, updatedUser)))
                .catch(error => reject(errorResponse()))
            })
            .catch(error => reject(error))
        });
    }

    this.getUserCount = function(query = {}, key = "count") {
        return new Promise((resolve, reject) => {
            collection.getUserCount(query)
            .then(count => { resolve(successResponse(200, { key: key, count: count })); })
            .catch(error => { reject(errorResponse(500, "Something went wrong. Please try again later.")); })
        })
    }

    this.resendVerify = function(id, email) {
        return new Promise((resolve, reject) => {
            collection.getById(id)
            .then(response => {
                if (response == null || response.status == "verified") { return; }
                verifier.sendUserVerify(id, email, `${scope}_limited`, root)
                .then(_ => resolve(successResponse(200, {})))
                .catch(_ => reject(errorResponse()));
            }).catch(_ => reject(errorResponse()));
        })
    }
 }

module.exports = {
    UserController,
    encryptPassword: encryptPassword,
    authenticate: authenticate
};