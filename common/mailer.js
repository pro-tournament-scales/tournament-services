var nodemailer = require('nodemailer');
const serverKey = process.env.FIR_API_KEY;
const request = require('request');
const fs = require('fs');
const logger = require('./logger');
const path = require('path');
const { google } = require("googleapis");
const OAuth2 = google.auth.OAuth2;
const oauth2Client = new OAuth2(
    process.env.GOOGLE_EMAIL_CLIENT_ID,
    process.env.GOOGLE_EMAIL_CLIENT_SECRET, 
    "https://developers.google.com/oauthplayground" // Redirect URL
);
oauth2Client.setCredentials({
    refresh_token: process.env.GOOGLE_EMAIL_REFRESH_TOKEN
});
const accessToken = oauth2Client.getAccessToken()

const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
         type: "OAuth2",
         user: process.env.DO_NOT_REPLY_EMAIL, 
         clientId: process.env.GOOGLE_EMAIL_CLIENT_ID,
         clientSecret: process.env.GOOGLE_EMAIL_CLIENT_SECRET,
         refreshToken: process.env.GOOGLE_EMAIL_REFRESH_TOKEN,
         accessToken: accessToken
    }
});

var send = function(to, subject, message, isHTML = false) {
    return new Promise(function(resolve, reject) { 
        var mailOptions = {
            from: `PTS <` + process.env.DO_NOT_REPLY_EMAIL + `>`,
            to: to.trim(),
            subject: subject
        };

        if (isHTML) { mailOptions.html = message; mailOptions.text = ""; } 
        else { mailOptions.text = message; }

        transporter.sendMail(mailOptions, function(error, info){
            if (error) { logger.warn(error); reject(error); } 
            else { logger.info(`-- email sent:  ${info.response} --`); resolve(info.response); }
        });
    });
}

var sendWithFile = function(to, subject, message, filePath, fileName, isHTML = false) {
    return new Promise(function(resolve, reject) { 
        var fileData = require("fs").readFileSync(path.resolve(__dirname, filePath));
        var mailOptions = {
            from: `PTS <` + process.env.DO_NOT_REPLY_EMAIL  + `>`,
            to: to.trim(),
            attachments: [
                {
                    filename: fileName,
                    content: Buffer(fileData),
                    cid: fileName,
                    contentType: 'application/pdf'
                }
            ],
            subject: subject
        };

        if (isHTML) { mailOptions.html = message; mailOptions.text = ""; } 
        else { mailOptions.text = message; }

        transporter.sendMail(mailOptions, function(error, info) {
            // delete file
            try { fs.unlinkSync(__dirname + filePath) } catch(err) { console.log(err) }
            if (error) { 
                reject(error);
                console.log(error);
            } else { 
                resolve(info.response);
                console.log(`-- email sent:  ${info.response} --`);
             }
        });
    });
}

var formatLinkForEmail = function(link) {
    return new Promise(function(resolve, reject) {
        const url = `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${serverKey}`;
        const dynamiclinkhost = process.env.DYNAMIC_LINK_HOST || "https://ptstest.page.link"
        const longDynamicLink = `${dynamiclinkhost}/?link=${link}`;
        request.post({url: url, form: {longDynamicLink: longDynamicLink}}, function(err,httpResponse,body) {
            if(err) { 
                logger.debug(`Failed to create short link for email: ${err}`);
                reject(err); 
                return; 
            }
            if (body == null) { reject("Failed to retrieve short link"); return;  }
            const resp = JSON.parse(body);
            if(resp == null || resp.shortLink == null) { 
                logger.debug(`Failed to create short link for email.`);
                resolve(longDynamicLink); 
                return; 
            }
            resolve(resp.shortLink);
        })
    });
}

var formatLinkForEmailWithKey = function(key, link) {
    return new Promise(function(resolve, reject) {
        formatLinkForEmail(link)
        .then(url => { resolve({key: key, url: url}); })
        .catch(error => { reject(error); })
    });
}

var formatLinksForEmail = function(keyedLinks) {
    var promises = [];
    keyedLinks.forEach(keyedLink => { promises.push(formatLinkForEmailWithKey(keyedLink.key, keyedLink.url)); });
    return Promise.all(promises);
}



module.exports = {
    send: send,
    formatLinkForEmail: formatLinkForEmail,
    formatLinksForEmail: formatLinksForEmail,
    sendWithFile: sendWithFile
};