const mongojs = require("mongojs");

function UserCollection(dbCollection, 
    transform = function(user) { return user; }, 
    modifiedTransform = function(user) { return user; },
    squashUser = function() { return {}; }) { 

    this.createUser = function(user) {
        return new Promise(function(resolve, reject) {
            dbCollection.insert(transform(user), function(err, resp) {
                if (err) { reject(err); return; }
                resolve(transform(resp));
            });
        });
    }

    this.updateUser = function(modifiedUser) {
        return new Promise(function(resolve, reject) {
            dbCollection.findAndModify({query: {_id: mongojs.ObjectId(modifiedUser._id)},
            update: { $set: modifiedTransform(modifiedUser) },
            new: true}, function(err, resp) {
                if (err) { reject(err); return; }
                resolve(transform(resp));
            });
        });
    }

    this.getById = function(id, shouldSquash = false) {

        var squashObj = {}
        if (shouldSquash) { squashObj = squashUser(); }

        return new Promise(function(resolve, reject) {
            dbCollection.findOne({_id: mongojs.ObjectId(id)}, squashObj, function(err, resp) {
                if (err) { reject(err); return; }
                resolve(transform(resp));
            });
        });
    }

    this.getUsersById = function(ids, shouldSquash = false) {
        var squashObj = {}
        if (shouldSquash) { squashObj = squashUser(); }

        return new Promise(function(resolve, reject) {
            dbCollection.find({ _id: { $in: ids.map(id => mongojs.ObjectId(id)) } }, squashObj, function(err, resp) {
                if (err) { reject(err); return; }
                resolve(resp.map(item => transform(item)));
            });
        });
    }

    this.getByEmail = function(email) {
        console.log("attempting to get by email...");
        return new Promise(function(resolve, reject) {
            dbCollection.findOne({email: { $regex: '^' + email + '$',  $options: 'i'} }, function(err, resp) {
                console.log("found user");
                if (err) { reject(err); return; }
                resolve(transform(resp));
            });
        });
    }

    // this.getByEmail = function(email) {
    //     return new Promise(function(resolve, reject) {
    //         dbCollection.findOne({$and:[{
    //             $text: { $search: email } },{
    //             email: {
    //                 $elemMatch: { $regex: '^' + email + '$',  $options: 'i'} }
    //             }]
    //         }, function(err, resp) {
    //                 if (err) { reject(err); return; }
    //                 resolve(transform(resp));
    //         });
    //     });
    // }
    
    this.getUsers = function(minified = false, query = { status: "verified"}) {
        return new Promise(function(resolve, reject) {
            var squashedUser = { };
            if (minified) { squashedUser = squashUser(); }
            dbCollection.find(query, squashedUser, function(err, resp) {
                if (err) { reject(err); return; }
                resp.forEach(element => { delete element.password; });
                resp.forEach(element => { element = transform(element); });
                resolve(resp);
            })
        })
    }

    this.searchUsers = function(firstName, lastName, minified = false) {
        return new Promise(function(resolve, reject) {
            var squashedUser = { };
            if (minified) { squashedUser = squashUser(); }
            let query = { status: "verified" }
            if (firstName != null) { query.firstName = { $regex: `^.*${firstName}.*$`,  $options: 'i'}; }
            if (lastName != null) { query.lastName = { $regex: `^.*${lastName}.*$`,  $options: 'i'}; }
            dbCollection.find(query, squashedUser, function(err, resp) {
                if (err) { reject(err); return; }
                resp.forEach(element => { delete element.password; });
                resp.forEach(element => { element = transform(element); });
                resolve(resp);
            })
        })
    }
    
    this.updateVerified = function(id) {
        return new Promise(function(resolve, reject) { 
            dbCollection.findAndModify({query: {_id: mongojs.ObjectId(id)},
                update: {$set: {status: "verified"}},
                new: true}, function(err, resp) {
                    if (err) { reject(err); return; }
                    resolve(transform(resp));
                });
        });
    }
    
    this.updateProfileIcon = function(id, iconUrl) {
        return new Promise(function(resolve, reject) { 
            dbCollection.findAndModify({query: {_id: mongojs.ObjectId(id)},
                update: {$set: {iconUrl: iconUrl}},
                new: true}, function(err, resp) {
                    if (err) { reject(err); return; }
                    resolve(transform(resp));
                });
        });
    }

    this.updateSafeSportUrl = function(id, safeSportUrl) {
        return new Promise(function(resolve, reject) { 
            dbCollection.findAndModify({query: {_id: mongojs.ObjectId(id)},
                update: {$set: {safeSportUrl: safeSportUrl}},
                new: true}, function(err, resp) {
                    if (err) { reject(err); return; }
                    resolve(transform(resp));
                });
        });
    }
    
    this.updatePassword = function(id, password) {
        return new Promise(function(resolve, reject) { 
            dbCollection.findAndModify({query: {_id: mongojs.ObjectId(id)},
            update: {$set: {password: password}},
            new: true}, function(err, resp) {
                if (err) { reject(err); return; }
                resolve(transform(resp));
            });
        });
    }

    this.updateDocumentStatus = function(id, documentsCompleted, documentStatusMessage) {
        return new Promise(function(resolve, reject) { 
            dbCollection.findAndModify({query: {_id: mongojs.ObjectId(id)},
            update: {$set: {documentsCompleted: documentsCompleted, documentStatusMessage: documentStatusMessage}},
            new: true}, function(err, resp) {
                if (err) { reject(err); return; }
                resolve(transform(resp));
            });
        });
    }

    this.getUserCount = function(query = {}) {
        return new Promise(function(resolve, reject) { 
            dbCollection.count(query, function(err, resp) {
                if (err) { reject(err); return; }
                resolve(resp);
            })
        })
    }
}

module.exports = UserCollection