const host = process.env.HOST || "http://localhost:8080";
const imageHost = process.env.IMAGE_HOST;
const fbProjectId = process.env.FIREBASE_PROJECT_ID
const fmKeyFilename = process.env.FIREBASE_KEY_FILENAME
const storageBucket = process.env.FIREBASE_STORAGE_BUCKET
const logger = require('./logger');
const fs = require('fs')
const Resize = require('./Resize');
const {Storage} = require('@google-cloud/storage');
const {format} = require('util');

const resize = new Resize("/tmp/images")
const storage = new Storage({
    projectId: fbProjectId,
    keyFilename: fmKeyFilename
});

const bucket = storage.bucket(storageBucket);

var isValidImageUrl = function(url) {
    if (host == "http://localhost:8080") { return true; }
    if(url != null && !(/^(https):\/\/[^ "]+$/.test(url))) {
        console.log("location.imageUrl : " + url);
        return false;
    }
    return true;
}

var removeOldUrl = function(newImageUrl, imageUrl) {
    if (imageUrl != null && newImageUrl != null) {
        // remove old image
        removeImage(imageUrl);
     }
}

/**
 * Upload the image file to Google Storage
 * @param {File} file object that will be uploaded to Google Storage
 */
const uploadFileToStorage = (id, key, file, rootFolder = "documents", secondaryFoler = "contents") => {
  return new Promise((resolve, reject) => {
    if (!file) {
      reject('No file provided');
    }

    let fileUpload = bucket.file(`${rootFolder}/${id}/${secondaryFoler}/${Date.now()}_${file.originalname}`);

    const blobStream = fileUpload.createWriteStream({
      metadata: {
        contentType: file.mimetype
      }
    });

    blobStream.on('error', (error) => {
      logger.warn(`failed to stream data buffer with error ${JSON.stringify(error)}`);
      reject('Something is wrong! Unable to upload at the moment.');
    });

    blobStream.on('finish', () => {
      // The public URL can be used to directly access the file via HTTP.
      fileUpload.makePublic();
      const url = format(`https://storage.googleapis.com/${bucket.name}/${fileUpload.name}`);
      resolve({key, url});
    });

    blobStream.end(file.buffer);
  });
}

/**
 * Upload the image file to Google Storage
 * @param {File} file object that will be uploaded to Google Storage
 */
const uploadImageToStorage = (id, key, file, rootFolder = "documents", secondaryFoler = "contents") => {
    return new Promise((resolve, reject) => {
      if (!file) {
        logger.warn("no file was present. Failed to upload image to storage.");
        reject("Missing required information. Please try again later.");
        return;
      }

      if (id == null || key == null) { 
        logger.debug("missing id or key when trying to upload image to storage");
        reject("Missing required information. Please try again later."); 
        return; 
      }
      let newFileName = `${id}_${key}_${Date.now()}`;
  
      let fileUpload = bucket.file(`${rootFolder}/${id}/${secondaryFoler}/${newFileName}`);

      const blobStream = fileUpload.createWriteStream({
        metadata: {
          contentType: 'image/png'
        }
      });
  
      blobStream.on('error', (error) => {
        logger.warn(`failed to upload image to storage ${error}`);
        reject('Something went wrong. Unable to upload image at the moment.');
      });
  
      blobStream.on('finish', () => {
        // The public URL can be used to directly access the file via HTTP.
        fileUpload.makePublic();
        const url = format(`https://storage.googleapis.com/${bucket.name}/${fileUpload.name}`);
        resolve({key, url});
      });
  
      resize.getDataBuffer(file.path, 640, 480)
      .then(function(buffer) {
        blobStream.end(buffer);
      })
      .catch(function(error) {
        logger.warn(`failed to upload image to storage ${error}`);
        reject('Something went wrong. Unable to upload image at the moment.');
      })
    });
  }

  /**
 * Upload the image file to Google Storage
 * @param {File} file object that will be uploaded to Google Storage
 */
const uploadIconToStorage = (customerId, key, file) => {
    return new Promise((resolve, reject) => {
      if (!file) {
        logger.warn("no file was present. Failed to upload icon to storage.");
        reject('No image file');
        return;
      }

      if (customerId == null || key == null) { 
        logger.debug("missing customerId or key when trying to upload icon to storage");
        reject("Missing required information. Please try again later."); 
        return; 
      }

      let newFileName = `${customerId}_${key}_${Date.now()}`;
  
      let fileUpload = bucket.file(`users/${customerId}/icons/${newFileName}`);

      const blobStream = fileUpload.createWriteStream({
        metadata: {
          contentType: 'image/png'
        }
      });
  
      blobStream.on('error', (error) => {
        logger.warn(`failed to upload image to storage ${error}`);
        reject('Something went wrong. Unable to upload icon at the moment.');
      });
  
      blobStream.on('finish', () => {
        // The public URL can be used to directly access the file via HTTP.
        fileUpload.makePublic();
        const url = format(`https://storage.googleapis.com/${bucket.name}/${fileUpload.name}`);
        resolve({key, url});
      });
  
      resize.getDataBuffer(file.path, 192, 192)
      .then(function(buffer) {
        blobStream.end(buffer);
      })
      .catch(function(error) {
        logger.warn(`failed to upload icon to storage ${error}`);
        reject('Something went wrong. Unable to upload icon at the moment.');
      })
    });
}

var removeImage = function(imageUrl) {
    if (imageUrl != null) {
        if (imageUrl.includes(host)) {
            let url = imageUrl.replace(host, "public")
            try { fs.unlinkSync(url) } catch(err) { console.log(err) }
        } else if (imageUrl.includes(imageHost)) {
            let url = imageUrl.replace(imageHost, "");
            const file = bucket.file(url);
            file.delete(function(err, apiResponse) {
                if(err) { console.log(err); return; }
                console.log("--- img deleted ---");
            }); 
        }
     } 
}

module.exports = {
    isValidImageUrl: isValidImageUrl,
    uploadImageToStorage: uploadImageToStorage,
    uploadIconToStorage: uploadIconToStorage,
    uploadFileToStorage: uploadFileToStorage,
    removeOldUrl: removeOldUrl,
    removeImage: removeImage
};