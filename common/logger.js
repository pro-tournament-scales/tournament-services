const winston = require('winston');
 
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, prettyPrint } = format;

let options = {
    info: {
      level: 'info',
      filename: `info.log`,
      handleExceptions: true,
      json: true,
      maxsize: 5242880, // 5MB
      maxFiles: 5,
      colorize: false,
    },
    warn: {
        level: 'warn',
        filename: `warn.log`,
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
    },
    error: {
        level: 'error',
        filename: `error.log`,
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
    },
    debug: {
      level: 'debug',
      handleExceptions: true,
      json: false,
      colorize: true,
    },
};

const logger = createLogger({
  format: combine(
    timestamp(),
    prettyPrint()
  ),
  defaultMeta: { service: 'user-service' },
  transports: [
    new transports.File(options.info),
    new transports.File(options.warn),
    new transports.File(options.error),
    new transports.Console(options.debug),
  ],
});
 
//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    format: winston.format.simple(),
  }));
}

function info(message, id, key) {
    if (process.env.NODE_ENV !== 'production') { console.log(formatMessage(message, id, key)); }
    else { logger.info(formatMessage(message, id, key)); }
}

function debug(message, id, key) { 
    if (process.env.NODE_ENV !== 'production') { console.log(formatMessage(message, id, key)); }
    else { logger.debug(formatMessage(message, id, key)); }
}

function warn(message, id, key) { 
  if (process.env.NODE_ENV !== 'production') { console.log(formatMessage(message, id, key)); }
  else { logger.warn(formatMessage(message, id, key)); }
}

function error(message, id, key) { 
  if (process.env.NODE_ENV !== 'production') { console.log(formatMessage(message, id, key)); }
  else { logger.error(formatMessage(message, id, key)); }
}

function formatMessage(message, id, key) {
  if (id != null) { 
    message = `${key || "key"}: ${id}: ${message}`
  }
  return message;
}

module.exports = {
    info: info,
    debug: debug,
    warn: warn,
    error: error
}