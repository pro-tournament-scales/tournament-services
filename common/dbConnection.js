const mongojs = require('mongojs');
const connection = process.env.DB_CONNECTION
const db = mongojs(connection);

module.exports = {
    db: db
};
