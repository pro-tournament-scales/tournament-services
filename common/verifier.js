const mailer = require('./mailer');
const domainHost = process.env.HOST;
const jwtsecret = process.env.JWT_SECRET;
const jwt = require('jsonwebtoken');

function sendUserVerify(id, email, scope, type) {
    return new Promise(function(resolve, reject) {
        /* create verify link */
        var options = {
            expiresIn: "30d"
        };

        const token = jwt.sign({ id: id, email: email, scope: scope}, jwtsecret, options);
        // create url
        const rawUrl = `${domainHost}/v1/${type}/verify/${token}`;
        /*  */
        mailer.formatLinkForEmail(rawUrl).then(function(verifyUrl) {
            mailer.send(email, "Enroll Your THSBA Account", `You're almost there! To finish general account enrollment, please verify your email here: ${verifyUrl} - note: if you are a returning user, this will re-enroll you for the new year.`)
            .then(function(response) { console.log(`-- verify email has been sent ${response} --`); resolve(); })
            .catch(function(error) { console.log(`-- failed to send email due to error ${error} --`); reject(error); });
        })
        .catch(function(error) { reject(error); });
    });
}

module.exports = {
    sendUserVerify: sendUserVerify
}