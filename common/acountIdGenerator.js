const { uuid } = require('uuidv4');

function generateNumber() {
    return uuid(); 
}

module.exports = { 
    generateNumber: generateNumber
}