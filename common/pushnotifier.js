const FCM = require('fcm-node');
const serverKey = process.env.FIREBASE_MESSAGING_KEY;
const fcm = new FCM(serverKey);
const logger = require('./logger');

function broadcastMessage(topic, title, message, data = null) {
    return new Promise(function(resolve, reject) {
        var fcmMessage = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
            to: "/topics/" + topic,
            notification: {
                title: title, 
                body: message
            },
            data: data
        };
        
        fcm.send(fcmMessage, function(err, response) {
            if (err) { reject(err); }
            else { 
                logger.debug(`--- notification sent ${response} ---`)
                resolve(response); 
            }
        });
    })
}

module.exports = { 
    broadcastMessage: broadcastMessage 
}