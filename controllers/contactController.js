let validator = require('../common/validator');
let mailer = require('../common/mailer');
let contactUsEmail = process.env.CONTACT_US_EMAIL

function successResponse(status = 200, body = {}) {
    return {
        status: status,
        body: body
    }
}

function errorResponse(status = 400, message = "Something went wrong. Please try again later.") {
    return {
        status: status,
        message: message
    }
}

function generalEnquiry(info) {
    return new Promise(function(resolve, reject) {
        if (info.name == null 
            || info.email == null
            || info.subject == null
            || info.message == null) {
                reject(errorResponse(400, "Missing required info. Please try again."));
                return;
        }


        if (!validator.isPersonNameValid(info.name)) {
            reject(errorResponse(400, "Please enter a valid name and try again."));
            return;
        }

        if (!validator.isEmailValid(info.email)) {
            reject(errorResponse(400, "Please enter a valid email address and try again."));
            return;
        }

        if (info.subject.length > 128) {
            reject(errorResponse(400, "The subject must be less than 128 characters. Please shorten the subject and try again."));
            return;
        }

        if (info.message.length > 512) {
            reject(errorResponse(400, "Messages may only be less than 512 characters. Please shorten your message and try again."));
            return;
        }

        mailer.send(contactUsEmail, `General Enquiry: ${info.subject}`, `
            <br><b>Email: ${info.email}</b><br>
            <b>Name: ${info.name}</b>
            <p>${info.message}</p>
        `, true)
        resolve(successResponse());
    })
}

module.exports = {
    generalEnquiry: generalEnquiry
}