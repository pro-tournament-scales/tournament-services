const collection = require(`../collections/tournament/tournamentCollection`);
const archiveCollection = require(`../collections/tournament/archiveTournamentCollection`);
const teamCollection = require(`../collections/team/teamCollection`);
const tournamentAppCollection = require(`../collections/tournamentAppCollection`);
const resultsCollection = require(`../collections/resultsCollection`);
const irregularResultsCollection = require(`../collections/resultsIrregularCollection`);
const validator = require('../common/validator');
const logger = require('../common/logger');
const payer = require('../square/payer');
const uploader = require('../common/uploader');
const pushnotifier = require('../common/pushnotifier');
const operations = require('../common/operations');

/* number of days after tournament to show upcoming */
const upcomingTournamentsBackdate = process.env.UPCOMING_TOURNAMENT_BACK_DATE || 3

const tournamentResultsFailedMsg = "Failed to upload tournament results. Please try again.";
const tournamentDetailsFailedMsg = "Failed to upload tournament details. Please try again.";

function successResponse(status = 200, body = {}) {
    return {
        status: status,
        body: body
    }
}

function errorResponse(status = 400, message = "Something went wrong. Please try again later.") {
    return {
        status: status,
        message: message
    }
}

function createTournament(data) {
    return new Promise(function(resolve, reject) {
        if (data == null) { reject(errorResponse(400, "Missing required information. Please try again.")); return; }
        if (data.divisionId == null) { reject(errorResponse(400, "Missing required divisionId. Please try again.")); return; }
        if (data.grade == null || !validator.isGradeValid(data.grade)) { reject(errorResponse(400, "Missing or invalid highschool grade. Please try again.")); return; }
        if (data.startDate == null || !validator.isDateValid(data.startDate) || data.endDate == null || !validator.isDateValid(data.endDate)) { reject(errorResponse(400, "Missing or invalid start or end date. Please try again.")); return; }
        if (data.title == null) { reject(errorResponse(400, "Missing required title. Please try again.")); return;  }
        if (data.description == null) { reject(errorResponse(400, "Missing required description. Please try again.")); return; }
        if (data.subtotal == null || isNaN(parseFloat(data.subtotal))) { reject(errorResponse(400, "Missing or invalid price. Please try again later.")); return; }
        if (data.address == null || !validator.isAddressValid(data.address)) { reject(errorResponse(400, "Missing or invalid address. Please try again.")); return; }
        
        /* trim data */
        operations.trim(data);

        /* confirm price is a double */
        let subtotal = parseInt(data.subtotal); // parseFloat(data.price).toFixed(2);
        let total = payer.calculateCostWithSquare(subtotal * 100);
        data.price = Number(total / 100);
        data.subtotal = Number(subtotal);
        /* create timestamp start and end dates */
        data.startTimestamp = new Date(data.startDate);
        var endDate = new Date(data.endDate);
        endDate.setHours(23);
        endDate.setMinutes(59);
        data.endTimestamp = endDate;
        /* retrieve division to validate existence */
        require('./divisionController').getDivision(data.divisionId)
        .then(function(division) {
            if (division == null) { reject(errorResponse(400, "Failed to retrieve division by id. Please try again.")); return; }
            collection.createTournament(data)
            .then(function(response) { resolve(successResponse(200, response)); })
            .catch(function(error) { 
                reject(errorResponse(500));
            })
        })
        .catch(function(error) { 
            reject(errorResponse(500));
        })
    });
}

function updateTournament(data) {
    return new Promise(function(resolve, reject) {
        if (data == null || data._id == null) { reject(errorResponse(400, "Missing required information. Please try again.")); return; }
        if (data.divisionId == null) { reject(errorResponse(400, "Missing required divisionId. Please try again.")); return; }
        if (data.grade == null || !validator.isGradeValid(data.grade)) { reject(errorResponse(400, "Missing or invalid highschool grade. Please try again.")); return; }
        if (data.startDate == null || !validator.isDateValid(data.startDate) || data.endDate == null || !validator.isDateValid(data.endDate)) { reject(errorResponse(400, "Missing or invalid start or end date. Please try again.")); return; }
        if (data.title == null) { reject(errorResponse(400, "Missing required title. Please try again.")); return;  }
        if (data.description == null) { reject(errorResponse(400, "Missing required description. Please try again.")); return; }
        if (data.subtotal == null || isNaN(data.subtotal)) { reject(errorResponse(400, "Missing or invalid price. Please try again later.")); return; }
        if (data.address == null || !validator.isAddressValid(data.address)) { reject(errorResponse(400, "Missing or invalid address. Please try again.")); return; }

        /* trim data */
        operations.trim(data);
        
        /* confirm price is a double */
        let subtotal = parseInt(data.subtotal); // parseFloat(data.price).toFixed(2);
        let total = payer.calculateCostWithSquare(subtotal * 100);
        data.price = Number(total / 100);
        data.subtotal = subtotal;
        /* create timestamp start and end dates */
        data.startTimestamp = new Date(data.startDate);
        var endDate = new Date(data.endDate);
        endDate.setHours(23);
        endDate.setMinutes(59);
        data.endTimestamp = endDate;

        /* update tournament  */
        collection.updateTournament(data)
        .then(tournament => { resolve(successResponse(200, tournament)); })
        .catch(error => { reject(errorResponse(500)); })
    });
}

/**
 * Mark a tournament open or closed for registration
 * @param id 
 */
function updateTournamentRegistrationStatus(id, isOpen) {
    return new Promise(function(resolve, reject) {
        if (id == null) { reject(errorResponse(400, "Missing required information. Please try again later.")); return; }
        if (isOpen != true && isOpen != false) { reject(errorResponse(400, "Invalid registration status. Please try again.")); return; }
        // update tournament isRegistrationOpen to false
        collection.updateTournamentRegistrationStatus(id, isOpen)
        .then(tournament => resolve(successResponse(200, tournament)))
        .catch(error => reject(errorResponse(500)));
    });
}

function getTournament(id) {
    return new Promise(function(resolve, reject) {
        collection.getTournament(id)
        .then(function(tournament) { 
            if (!tournament) { 
                archiveCollection.getTournament(id)
                .then(archivedTournament => {
                    resolve(successResponse(200, checkAndApplyLateFees(archivedTournament))); 
                })
                .catch(error => resolve(error))
                return;
            }
            resolve(successResponse(200, checkAndApplyLateFees(tournament))) 
        })
        .catch(function(error) { reject(errorResponse(500, "Failed to find tournament by id. Please try again later.")) });
    });
}

function getArchivedTournament(id) {
    return new Promise(function(resolve, reject) {
        archiveCollection.getTournament(id)
        .then(function(tournament) { 
            resolve(successResponse(200, checkAndApplyLateFees(tournament))) 
        })
        .catch(function(error) { reject(errorResponse(500, "Failed to find tournament by id. Please try again later.")) });
    });
}

function deleteTournament(id) {
    return new Promise(function(resolve, reject) {
        collection.deleteTournament(id)
        .then(response => { 
            resolve(successResponse(200, { deleted: true }));
            /* remove teams */
            teamCollection.getTeamsForTournament(id)
            .then(teams => {
                /* should we notify team primary anglers of deletion? */
                teams.forEach(team => { 
                    if(team.primaryAngler != null && team.primaryAngler.id != null) {
                        pushnotifier.broadcastMessage(team.primaryAngler.id, "Tournament Removed by THSBA", "THSBA has removed a tournament that was tied to one of your teams. Your team has also been removed. Please contact us if you have any questions.")
                    }
                    teamCollection.deleteTeam(team._id);
                });
            })
            .catch(error => { logger.debug("failed to remove teams for tournament"); })
        })
        .catch(error => { reject(errorResponse(500, "Failed to find tournament by id. Please try again later.")) });
    });
}

function getTournamentsForDivision(divisionId) {
    return new Promise(function(resolve, reject) {
        collection.getTournamentsForDivision(divisionId)
        .then(function(tournaments) { 
            tournaments = tournaments.map(tournament => checkAndApplyLateFees(tournament));
            resolve(successResponse(200, tournaments)); 
        })
        .catch(function(error) { reject(errorResponse(500)) });
    });
}

function getTournamentsById(ids) {
    return new Promise((resolve, reject) => {
        if (ids == null || ids.length == 0) { resolve(successResponse(200, [])); return; }
        Promise.all([
            collection.getTournamentsById(ids),
            archiveCollection.getTournamentsById(ids)
        ]
        ).then(tournaments => { 
            let flattenedTournaments = operations.flatten(tournaments);
            resolve(successResponse(200, flattenedTournaments)) 
        }).catch(error => {
            reject(errorResponse(500)); 
        });
    });
}

function getUpcomingTournaments() {
    return new Promise(function(resolve, reject) {
        let currentDate = new Date()
        let date = new Date(currentDate - 1000 * 60 * 60 * 24 * upcomingTournamentsBackdate);
        collection.getUpcomingTournaments(date)
        .then(function(tournaments) { 
            tournaments = tournaments.map(tournament => checkAndApplyLateFees(tournament));
            resolve(successResponse(200, tournaments)); 
        })
        .catch(function(error) { reject(errorResponse(500)); });
    })
}

function getUpcomingTournamentsForDivision(divisionId) {
    return new Promise(function(resolve, reject) {
        let currentDate = new Date()
        let date = new Date(currentDate - 1000 * 60 * 60 * 24 * upcomingTournamentsBackdate);
        collection.getUpcomingTournamentsForDivision(divisionId, date)
        .then(function(tournaments) { 
            tournaments = tournaments.map(tournament => checkAndApplyLateFees(tournament));
            resolve(successResponse(200, tournaments));
         })
        .catch(function(error) { 
            reject(errorResponse(500));
         });
    })
}

function getPastTournaments() {
    return new Promise(function(resolve, reject) {
        let date = new Date();
        collection.getPastTournaments(date)
        .then(function(tournaments) { 
            if (!tournaments || tournaments.length == 0) {
                getArchivedTournaments().then(response => {
                    let archivedTournaments = response.body;
                    archivedTournaments.map(archivedTournament => checkAndApplyLateFees(archivedTournament));
                    resolve(successResponse(200, archivedTournaments));
                })
                .catch(error => reject(error));
                return;
            }
            tournaments = tournaments.map(tournament => checkAndApplyLateFees(tournament));
            resolve(successResponse(200, tournaments)); 
        })
        .catch(function(error) { reject(errorResponse(500)) });
    })
}

function getPastTournamentsForDivision(divisionId) {
    return new Promise(function(resolve, reject) {
        let date = new Date();
        collection.getPastTournamentsForDivision(divisionId, date)
        .then(function(tournaments) { 
            tournaments = tournaments.map(tournament => checkAndApplyLateFees(tournament));
            resolve(successResponse(200, tournaments));
         })
        .catch(function(error) { 
            reject(errorResponse(500));
         });
    })
}

function getArchivedTournaments() {
    return new Promise((resolve, reject) => {
        archiveCollection.getTournaments()
        .then((tournaments) => {
            resolve(successResponse(200, tournaments));
        })
        .catch(error => {
            reject(errorResponse(500));
        })
    })
}

function uploadTournamentDetails(id, doc) {
    return new Promise(function(resolve, reject) {
        uploader.uploadFileToStorage(id, "tournament_details", doc, "tournaments", "documents")
        .then(result => {
            /* retrieve existing profile iconUrl and delete */
            collection.getTournament(id).then(function(response) {
                if (response == null) { reject(errorResponse(500, tournamentDetailsFailedMsg)); return; }
                uploader.removeImage(response.detailsUrl); // remove existing image from storage if exists
                collection.updateTournamentDetailsUrl(id, result.url).then(function(response) {
                    resolve(successResponse(200, response));    
                }).catch(function(error) {
                    logger.debug(`failed to update tournament with details url ${result.url}`)
                    uploader.removeImage(result.url);
                    reject(errorResponse(500, tournamentDetailsFailedMsg));
                })
            })
            .catch(function(error) {
                logger.debug(`Tournament details failed to upload with error: ${JSON.stringify(error)}`);
                uploader.removeImage(result.url);
                reject(errorResponse(500, tournamentDetailsFailedMsg));
            });
        })
        .catch(error => {
            logger.warn(`Tournament details failed to upload with error: ${JSON.stringify(error)}`);
            reject(errorResponse(500, tournamentDetailsFailedMsg));
        })
    });
}

function uploadTournamentResults(id, doc) {
    return new Promise(function(resolve, reject) {
        uploader.uploadFileToStorage(id, "tournament_result", doc, "tournaments", "documents")
        .then(result => {
            /* retrieve existing profile iconUrl and delete */
            collection.getTournament(id).then(function(response) {
                if (response == null) { reject(errorResponse(500, tournamentResultsFailedMsg)); return; }
                uploader.removeImage(response.resultsUrl); // remove existing image from storage if exists
                collection.updateTournamentResultsUrl(id, result.url).then(function(response) {
                    resolve(successResponse(200, response));    
                }).catch(function(error) {
                    logger.debug(`failed to update tournament with results url ${result.url}`)
                    uploader.removeImage(result.url);
                    reject(errorResponse(500, tournamentResultsFailedMsg));
                })
            })
            .catch(function(error) {
                logger.debug(`Tournament results failed to upload with error: ${JSON.stringify(error)}`);
                uploader.removeImage(result.url);
                reject(errorResponse(500, tournamentResultsFailedMsg));
            });
        })
        .catch(error => {
            logger.warn(`Tournament results failed to upload with error: ${JSON.stringify(error)}`);
            reject(errorResponse(500, tournamentResultsFailedMsg));
        })
    });
}

function uploadTournamentHighschoolResults(id, doc) {
    return new Promise(function(resolve, reject) {
        uploader.uploadFileToStorage(id, "tournament_highschool_result", doc, "tournaments", "documents")
        .then(result => {
            /* retrieve existing profile iconUrl and delete */
            collection.getTournament(id).then(function(response) {
                if (response == null) { reject(errorResponse(500, tournamentResultsFailedMsg)); return; }
                uploader.removeImage(response.highschoolResultsUrl); // remove existing image from storage if exists
                collection.updateTournamentHighschoolResultsUrl(id, result.url).then(function(response) {
                    resolve(successResponse(200, response));    
                }).catch(function(error) {
                    logger.debug(`failed to update tournament with highschool results url ${result.url}`)
                    uploader.removeImage(result.url);
                    reject(errorResponse(500, tournamentResultsFailedMsg));
                })
            })
            .catch(function(error) {
                logger.debug(`Highscool tournament results failed to upload with error: ${JSON.stringify(error)}`);
                uploader.removeImage(result.url);
                reject(errorResponse(500, tournamentResultsFailedMsg));
            });
        })
        .catch(error => {
            logger.warn(`Highscool tournament results failed to upload with error: ${JSON.stringify(error)}`);
            reject(errorResponse(500, tournamentResultsFailedMsg));
        })
    });
}


function checkAndApplyLateFees(tournament) {
    if (tournament != null && tournament.startTimestamp != null) {
        tournament.isLate = false;
        let startDate = new Date(tournament.startTimestamp);
        let response = payer.checkTournamentLateFee(startDate, tournament.subtotal)
        tournament.price = response.readable_total;
        tournament.subtotal = response.readable_subtotal;
        if (response.status == "late" || response.status == "expired") { tournament.isLate = true; }
    }
    return tournament;
}

function uploadResultsData(data) {
    return new Promise(function(resolve, reject) {
        if (data == null) { reject(errorResponse(400, "Missing required information. Please try again.")); return; }
        logger.debug("before trim...");

        /* trim data */
        operations.trim(data);

        /* split data into manual vs imported teams */
        var importedTeams = [];
        var manualTeams = [];

        for (var result of data.results) {
            if (result.manualEntryData) {
                manualTeams.push(result);
            } 

            if (!result.manualEntryData || result.manualEntryData.type == "add_angler") {
                let importResult = {
                    teamId: result.teamId,
                    weight: result.weight,
                    bigFish: result.bigFish,
                    totalFish: result.totalFish,
                    fishDead: result.fishDead,
                    disqualified: result.disqualified
                }

                importedTeams.push(importResult)
            }
        }

        /* update tournament data for imported teams  */
        resultsCollection.updateResults({
            tournamentId: data.tournamentId,
            results: importedTeams
        }).then(_ => { 
            /* update tournament data for manual entry teams */
            irregularResultsCollection.updateResults({
                tournamentId: data.tournamentId,
                results: manualTeams })
                .then(results => { resolve(successResponse(200, results)); })
                .catch(error => { 
                    logger.warn(`error - ${error}`)
                    reject(errorResponse(500)); 
                })  
        }).catch(error => { 
            logger.warn(`error - ${error}`)
            reject(errorResponse(500)); })
    });
}

function uploadAppTournamentData(data) {
    return new Promise(function(resolve, reject) {
        if (data == null) { reject(errorResponse(400, "Missing required information. Please try again.")); return; }
        logger.debug("before trim...");

        /* trim data */
        operations.trim(data);

        /* update tournament  */
        tournamentAppCollection.updateTournament(data)
        .then(tournament => { resolve(successResponse(200, tournament)); })
        .catch(error => { 
            logger.warn(`error - ${error}`)
            reject(errorResponse(500)); })

    })
}

fishWeighed = (tournament) => {
    var totalFish = 0;
    tournament.results.forEach(result => {
        result.weighins.forEach(weighin => {
            totalFish += weighin.total
        })
    });
    return totalFish;
}

totalWeighins = (tournament) => {
    var totalWeighins = 0;

    tournament.results.forEach(result => {
        totalWeighins += result.weighins.length;
    });

    return totalWeighins;

}

function getTournamentSummmaryData(resolve, reject) {
     /* get all tournaments  */
     tournamentAppCollection.getAllTournaments()
     .then(tournaments => { 
         var response = []
         tournaments.forEach(tournament => {
             var item = {
                 _id : tournament._id,
                 title: tournament.title,
                 description: tournament.description,
                 startDate: tournament.startDate,
                 endDate: tournament.endDate,
                 totalTeams: tournament.teams.length,
                 fishWeighed: fishWeighed(tournament),
                 totalWeighins: totalWeighins(tournament)
             }
             response.push(item)
         });
         
         resolve(successResponse(200, response)); })
     .catch(error => { 
         logger.warn(`error - ${error}`)
         reject(errorResponse(500)); })
}

function getAppTournamentData(id) {
    return new Promise(function(resolve, reject) {
        if (id) return tournamentAppCollection.getTournament(id).then(response => resolve(successResponse(200, response)))
        else return getTournamentSummmaryData(resolve, reject)
    })
}

function archivePastTournaments() {
    return new Promise((resolve, reject) => {
        collection.getPastTournaments(new Date())
        .then(tournaments => {
            if (tournaments.length == 0) { resolve(); return; }
            // add tournaments to archive, then remove from standard collection
            tournaments.forEach(tournament => {
                archiveCollection.createTournament(tournament)
                .then(result => {
                    // remove current collection
                    collection.deleteTournament(result._id)
                    .then(response => { 
                        console.log("Tournament archived and deleted") 
                        resolve();
                    })
                    .catch(error => logger.warn(error));
                })
                .catch(error => { logger.warn(error) });
            })
        })
    })
}

module.exports = {
    createTournament: createTournament,
    updateTournament: updateTournament,
    getTournament: getTournament,
    getArchivedTournament: getArchivedTournament,
    deleteTournament: deleteTournament,
    getTournamentsById: getTournamentsById,
    getTournamentsForDivision: getTournamentsForDivision,
    getUpcomingTournaments: getUpcomingTournaments,
    getUpcomingTournamentsForDivision: getUpcomingTournamentsForDivision,
    getPastTournaments: getPastTournaments,
    getPastTournamentsForDivision: getPastTournamentsForDivision,
    getArchivedTournaments: getArchivedTournaments,
    uploadTournamentResults: uploadTournamentResults,
    uploadTournamentHighschoolResults: uploadTournamentHighschoolResults,
    uploadTournamentDetails: uploadTournamentDetails,
    updateTournamentRegistrationStatus: updateTournamentRegistrationStatus,
    uploadAppTournamentData: uploadAppTournamentData,
    getAppTournamentData: getAppTournamentData,
    uploadResultsData: uploadResultsData,
    archivePastTournaments: archivePastTournaments
}