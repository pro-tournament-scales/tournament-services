const uploader = require('../common/uploader');
const collection = require('../collections/documentCollection');

const missingDocumentPaths = "Missing document paths. Please try again.";

function successResponse(status = 200, body = {}) {
    return {
        status: status,
        body: body
    }
}

function errorResponse(status = 400, message = "Something went wrong. Please try again later.") {
    return {
        status: status,
        message: message
    }
}

function uploadDocument(userid, name, document) {
    return new Promise(function(resolve, reject) {
        if (userid == null || name == null || document == null) { reject(errorResponse(400, "Missing required information. Please try again later.")); return; }
        uploader.uploadFileToStorage(userid, name, document)
        .then(storageResult => { 
            /* store url in documents object */
            collection.getDocuments(userid)
            .then(result => { 
                /*  when a user already has documents, update paths */
                if (result != null && result.paths != null && result.paths.length > 0) {
                    var paths = result.paths;
                    paths = paths.push(storageResult);
                    collection.updateDocuments(userid, paths)
                    .then(resp => { resolve(successResponse(200, resp)); })
                    .catch(error => { reject(errorResponse()); });
                    return;
                }

                /* when a user has no documents, create document with path */
                collection.createDocument(userid, storageResult)
                .then(resp => { resolve(successResponse(200, resp)); })
                .catch(error => { reject(errorResponse()); })
            })
         })
        .catch(error => { reject(errorResponse()); })
    });
}

function getDocuments(userid) {
    return new Promise(function(resolve, reject) {
        collection.getDocuments(userid)
        .then(resp => { resolve(successResponse(200, resp)); })
        .catch(error => { reject(errorResponse()); })
    });
}

function deleteDocument(userid, path) {
    return new Promise(function(resolve, reject) {
        collection.getDocuments(userid)
        .then(resp => { 
            /* find document by matching path and remove from paths */
            if (resp == null || resp.paths == null || resp.paths.length <= 0) { reject(errorResponse(400, missingDocumentPaths));  return; }
            let index = resp.paths.findIndex(function(element) { return element.path == path; })
            let updatedPaths = resp.paths.splice(index, 1);
            collections.updateDocuments(userid, updatedPaths)
            .then(updatedDoc => { resolve(successResponse(200, updatedDoc)); })
            .catch(error => { reject(errorResponse()); })
        })
        .catch(error => { reject(errorResponse()); })
    });
}

module.exports = {
    uploadDocument: uploadDocument,
    getDocuments: getDocuments,
    deleteDocument: deleteDocument
}