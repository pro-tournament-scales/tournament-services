const collection = require("../collections/betaCollection")


function successResponse(status = 200, body = {}) {
    return {
        status: status,
        body: body
    }
}

function errorResponse(status = 400, message = "Something went wrong. Please try again later.") {
    return {
        status: status,
        message: message
    }
}

function getActiveCampaigns() {
    return new Promise(function (resolve, reject) {
        collection.getActiveCampaigns()
        .then(function(campaigns) { resolve(successResponse(200, campaigns )); })
        // .catch(function(error) { reject(errorResponse(500, "Something went wrong. Please try again later.")); });
    })
}

module.exports = {
    getActiveCampaigns: getActiveCampaigns
}