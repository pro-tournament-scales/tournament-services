//
// # Server
//
// Server managing all routed http requests
//
var server;
require('dotenv').config();
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const apiKey = require('./authentication/apiKey');
const mailer = require('./common/mailer');
const logger = require('./common/logger');
const unauthorizedHandler = require('./authentication/response/unauthorizedHandler');
const loggingMiddleware = require('./authentication/middleware/loggingMiddleware');

const PORT = process.env.PORT || 3000;

/* callback delegates */
var onServerStarted = () => { };
var onServerEnded = () => { };

app.use(express.static(__dirname + "/public"));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(apiKey); // validate application by api key 
app.use(unauthorizedHandler); // handle all 401
app.use(loggingMiddleware); // log usage

server = app.listen(PORT, function () {
  logger.debug(`Server listening on port ${PORT}`);
  onServerStarted();
});


require('./resources/userResource')(app);
require('./resources/betaResource')(app);


/* handle uncaught errors */
process.on("uncaughtException", function(err) {
  onServerEnded();
  if(process.env.NODE_ENV === "production") {
    mailer.send(process.env.SUPPORT_EMAIL, "Uncaught Exception", err);
    logger.error(err);
  } else {
    logger.error(err);
    process.exit(1);
  }
});

/**
 * 
 * @param {*} done callback to determine when server has completed close
 * manually kill the server process
 */
function close(done = {}) { 
  if (server) server.close(done) 
}

module.exports = { 
  onServerStarted: onServerStarted, 
  onServerEnded: onServerEnded,
  close: close 
}