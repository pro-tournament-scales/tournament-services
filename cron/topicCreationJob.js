const anglerCollection = require('../collections/anglerCollection');
const boatcaptainCollection = require('../collections/boatCaptainCollection');
const observerCollection = require('../collections/observerCollection');
const fanparentCollection = require('../collections/fanParentCollection');
const hsAdminCollection = require('../collections/highschoolAdminCollection');
const subscriptionController = require('../controllers/subscriptionController');
const logger = require('../common/logger');

const run = () => {
    Promise.all([
        anglerCollection.getUsers(),
        boatcaptainCollection.getUsers(),
        observerCollection.getUsers(),
        fanparentCollection.getUsers(),
        hsAdminCollection.getUsers()
    ])
    .then(users => {
        users = users.reduce(function(prev, curr) {
            return prev.concat(curr);
        });

        users.forEach(user => { 
            subscriptionController.subscribe("global", user._id.toString()) 
        })
    })
    .catch(error => logger.warn(`topic creator job failed with error: ${JSON.stringify(error)}`))
}

module.exports = { run: run }