const anglerCollection = require('../collections/anglerCollection');
const boatCaptainCollection = require('../collections/boatCaptainCollection');
const observerCollection = require('../collections/observerCollection');
const boatCaptainController = require('../controllers/user/boatCaptainController');
const observerController = require('../controllers/user/observerController');
const logger = require('../common/logger');
const cronJob = require('cron').CronJob;

// var job = new cronJob({
//    cronTime: '0 0 0  7 1 *',
//     onTick: function() {
//     // unenroll();
//   },
//   timeZone: 'America/Monterrey'
// });
// job.start();

function unenroll() {
    anglerCollection.unenrollAnglers()
    .then(response => logger.warn(`Anglers have been unenrolled - size ${JSON.stringify(response)}`))
    .catch(error => logger.warn(`Failed to unenroll anglers - ${error}`))

    boatCaptainCollection.unenrollCaptains()
    .then(response => logger.warn(`Captains have been unenrolled - size ${JSON.stringify(response)}`))
    .catch(error => logger.warn(`Failed to unenroll captains - ${error}`))

    observerCollection.unenrollObservers()
    .then(response => logger.warn(`Observers have been unenrolled - size ${JSON.stringify(response)}`))
    .catch(error => logger.warn(`Failed to unenroll observers - ${error}`))

    boatCaptainController.removeAllSafeSportUrls();
    observerController.removeAllSafeSportUrls();

    // archive all tournaments and teams
    require('../controllers/tournamentController')
    .archivePastTournaments()
    .then(require('../controllers/teamController')
    .archiveTeamsForArchivedTournaments());

    // require('./controllers/tournamentController').archivePastTournaments()
// .then(require('./controllers/teamController').archiveTeamsForArchivedTournaments());
}

module.exports = {
    unenroll: unenroll
}