const teamCollection = require('../collections/team/teamCollection');
const anglerCollection = require('../collections/anglerCollection');
const boatCaptainCollection = require('../collections/boatCaptainCollection');
const observerCollection = require('../collections/observerCollection');
const logger = require('../common/logger');
const cronJob = require('cron').CronJob;

var job = new cronJob({
   cronTime: '0 0 0 * * *',
    onTick: function() {
    cleanTeams();
  },
   timeZone: 'America/Monterrey'
});
job.start();

function cleanTeams() {
    /* get all teams */
    teamCollection.getTeams()
    .then(teams => {
        teams.forEach(team => {
            /* check primary angler */
            if (team.primaryAngler != null && team.primaryAngler.id != null) {
                anglerCollection.getById(team.primaryAngler.id)
                .then(angler => { 
                    if (angler == null) { 
                        logger.warn(`Primary angler ${team.primaryAngler.id} no longer exists on team ${team._id}`);
                    } 
                })
                .catch(error => { logger.warn("An error occured when retrieving primary angler in teamCleanJob"); });
            }

            /* check secondary angler */
            if (team.secondaryAngler != null && team.secondaryAngler.id != null && team.secondaryAngler.id != "") {
                anglerCollection.getById(team.secondaryAngler.id)
                .then(angler => { 
                    if (angler == null) {
                        _removeMemberFromTeam(team, team.secondaryAngler.id, "secondary_angler"); 
                    } 
                })
                .catch(error => { logger.warn(`An error occured when retrieving secondary angler in teamCleanJob ${error}`); })
            } else if (team.secondaryAngler != null && (team.secondaryAngler.id === null || team.secondaryAngler.id == "")) {
                _removeMemberFromTeam(team, `null or empty: ${team.secondaryAngler.id}`, "secondary_angler"); 
            } else if(team.secondaryAngler === null) {
                _removeMemberFromTeam(team, `entire object null or empty `, "secondary_angler"); 
            }

            /* check boat captain */
            if (team.boatCaptain != null && team.boatCaptain.id != null && team.boatCaptain.id != "") {
                boatCaptainCollection.getById(team.boatCaptain.id)
                .then(boatcaptain => { 
                    if (boatcaptain == null) {
                        _removeMemberFromTeam(team, team.boatCaptain.id, "boat_captain"); 
                    } 
                })
                .catch(error => { logger.warn(`An error occured when retrieving boat captain in teamCleanJob ${error}`); })
            } else if (team.boatCaptain != null && (team.boatCaptain.id === null || team.boatCaptain.id == "")) {
                _removeMemberFromTeam(team, `null or empty ${team.boatCaptain.id}`, "boat_captain"); 
            } else if(team.boatCaptain === null) {
                _removeMemberFromTeam(team, `entire object null or empty `, "boat_captain"); 
            }

            /* check observer */
            if (team.observer != null && team.observer.id != null && team.observer.id != "") {
                observerCollection.getById(team.observer.id)
                .then(observer => { 
                    if (observer == null) { 
                        /* check if observer is a boat captain */
                        if (team.boatCaptain == null || team.boatCaptain.id == null) { _removeMemberFromTeam(team, team.observer.id, "observer"); return; }
                        boatCaptainCollection.getById(team.boatCaptain.id)
                        .then(boatcaptain => { 
                            if (boatcaptain == null) {
                                _removeMemberFromTeam(team, team.observer.id, "observer"); 
                            } 
                        })
                        .catch(error => { logger.warn(`An error occured when retrieving boat captain for observer in teamCleanJob ${error}`); })
                    } 
                })
                .catch(error => { logger.warn(`An error occured when retrieving observer for id ${team.observer.id} with teamId ${team._id} in teamCleanJob ${error}`); })
            } else if (team.observer != null && (team.observer.id === null || team.observer.id == "")) {
                _removeMemberFromTeam(team, `null or empty ${team.observer.id}`, "observer"); 
            } else if(team.observer === null) {
                _removeMemberFromTeam(team, `entire object null or empty `, "observer"); 
            }
        })
    })
    .catch(error => logger.error(`Failed to run team clean job ${error}`));
}

function _removeMemberFromTeam(team, userRefId, memberType) {
    console.log(`remove member ${memberType} with id ${userRefId} from team ${team._id}`);
    switch (memberType) {
        case "secondary_angler":
        teamCollection.updateTeamRemoveSecondaryAngler(team._id);
        break;
        case "boat_captain":
        teamCollection.updateTeamRemoveBoatCaptain(team._id);
        break;
        case "observer":
        teamCollection.updateTeamRemoveObserver(team._id);
        break;
    }
}

module.exports = {
    cleanTeams: cleanTeams
}