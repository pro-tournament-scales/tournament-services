const squareDomain = process.env.SQUARE_DOMAIN;
const squareVersion = process.env.SQUARE_VERSION;
const squareAccessToken = process.env.SQUARE_ACCESS_TOKEN;
const squareFlatFee = process.env.SQUARE_FLAT_FEE || 30;
const sqaurePercentFee = process.env.SQURE_PERCENT_FEE || 0.029;
const tournamentLateFeeCost = process.env.TOURNAMENT_LATE_FEE_COST || "20";
const lateFeeDays = process.env.TOURNAMENT_LATE_FEE_DAYS || 5;
const expiredDays = process.env.TOURNAMENT_EXPIRED_DAYS || 2;
const { uuid } = require('uuidv4');
const request = require('request');
const logger = require('../common/logger');
const operations = require('../common/operations');

function makePayment(nonce, amount, referenceId, note = "") {
    return new Promise(function(resolve, reject) { 
        if (nonce == null || amount == null || isNaN(amount)) {
            reject("Something went wrong. Please try again later.");
            return;
        }

        /* generate idempotency key to uniquely distinguish req */
        const idempotencyKey = uuid();
        /* verify amount is valid */
        if (isNaN(amount)) { reject("Please enter a valid amount and try again."); return; }
            var options = {
                url: `${squareDomain}/v2/payments`,
                headers: { 
                    'Authorization': `Bearer ${squareAccessToken}`, 
                    'Square-Version': squareVersion,
                    'Content-Type': `application/json`
                    },
                body: {
                    'idempotency_key': idempotencyKey,
                    'amount_money': {
                        'amount': amount,
                        'currency': 'USD'
                    },
                    'source_id': nonce,
                    'reference_id': referenceId,
                    'note': note,
                    'autocomplete': true
                    },
                json: true
            };

            /* handle callback result */
            function callback(error, response, body) {
                if (!error && (response.statusCode == 200) && body != null) {
                    logger.info("user has made a payment")
                    logger.debug(response);
                    resolve(body)
                } else { 
                    if (response != null) { logger.warn(`Payment failed with response ${JSON.stringify(response)}`); }
                    if (error != null) { logger.warn(`Payment failed with error ${JSON.stringify(error)}`); }
                    reject(error || "Something went wrong when processing the payment. Please try again later.");
                }
            }

            /* make internal request, set auth headers */
            request.post(options, callback);
        });
}

function retrievePayments(begin_time, end_time) {
    var url = `${squareDomain}/v2/payments`

    if (begin_time != null && end_time != null) {
        url = url.concat(`?begin_time=${begin_time}&end_time=${end_time}`);
    }

    return new Promise((resolve, reject) => {
        var options = {
            url: url,
            headers: { 
                'Authorization': `Bearer ${squareAccessToken}`, 
                'Square-Version': squareVersion,
                'Content-Type': `application/json`
                },
            json: true
        };

        /* handle callback result */
        function callback(error, response, body) {
            if (!error && (response.statusCode == 200) && body != null) {
                logger.info("payments retrieved")
                logger.debug(response);
                resolve(body)
            } else { 
                if (response != null) { logger.warn(`Payment retrieval failed with response ${JSON.stringify(response)}`); }
                if (error != null) { logger.warn(`Payment retrieval failed with error ${JSON.stringify(error)}`); }
                reject(error || "Something went wrong when retrieving payments. Please try again later.");
            }
        }

        /* make internal request, set auth headers */
        request.get(options, callback);
    })
}

function calculateCostWithSquare(originalCost) {
    if (isNaN(originalCost)) { 
        logger.error("Failed to calculate cost for sqaure payment")
        return 0;
    }
    // 2.9% + 30¢ 
    // 6000 * .029 = 174
    // 6174 + 30 - 6204
    originalCost = parseFloat(originalCost);

    let total = originalCost
    + (originalCost * sqaurePercentFee)
    + squareFlatFee
    return parseInt(total);
}

function checkTournamentLateFee(startDate, subtotal, currentDate = operations.calcTime(-5)) {
    let response = { status: "none", total: 0, readable_total: 0, subtotal: 0, readable_subtotal: 0};
    if (startDate == null || subtotal == null) { return response; }
    let lateFeeDate = new Date(startDate - 1000 * 60 * 60 * 24 * lateFeeDays);
    let expiredDate = new Date(startDate - 1000 * 60 * 60 * 24 * expiredDays);
    /* compare dates... if < 6 days difference then add $20 fee */
    if (currentDate >= lateFeeDate) {
        let lateFeeSubTotal = subtotal + parseInt(tournamentLateFeeCost)
        let total = calculateCostWithSquare(lateFeeSubTotal * 100);
        let readable_total = Number(total / 100);
        var status = "late";
        if (currentDate >= expiredDate) { status = "expired"; }
        return {
            status: status,
            total: total,
            readable_total: readable_total,
            readable_late_fee_subtotal: lateFeeSubTotal,
            late_fee_subtotal: lateFeeSubTotal * 100,
            subtotal: subtotal * 100,
            readable_subtotal: subtotal
        }
    } else {
        let total = calculateCostWithSquare((subtotal) * 100);
        let readable_total = Number(total / 100);
        return {
            status: "none",
            total: total,
            readable_total: readable_total,
            subtotal: subtotal * 100,
            readable_subtotal: subtotal
        }
    }
}

module.exports = { 
    makePayment: makePayment,
    retrievePayments: retrievePayments,
    checkTournamentLateFee: checkTournamentLateFee,
    calculateCostWithSquare: calculateCostWithSquare
}