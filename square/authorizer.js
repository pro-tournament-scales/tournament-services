const squareDomain = process.env.SQUARE_DOMAIN;
const squareApplicationId = process.env.SQUARE_APPLICATION_ID;
const squareApplicationSecret = process.env.SQUARE_APPLICTATION_SECRET;
const squareVersion = process.env.SQUARE_VERSION;
const request = require('request');

function authorize() {
    return new Promise(function(resolve, reject) { 
        var options = {
            url: `${squareDomain}/oauth2/authorize?client_id=${squareApplicationId}&scope=PAYMENTS_READ PAYMENTS_WRITE`
        };

        /* handle callback result */
        function callback(error, response, body) {
            if (!error && (response.statusCode == 200) && body != null && body.code != null) {
                resolve(body.code)
            } else { 
                reject(error || "Something went wrong when authorizing. Please try again later.");
            }
        }

        /* make internal request, set auth headers */
        request.get(options, callback);
    });
}

function obtainToken(code) {
    return new Promise(function(resolve, reject) { 
        if (code == null) {
            reject("Missing required information. Please try again.");
            return;
        }

        var options = {
            url: `${squareDomain}/v2/payments`,
            headers: { 
                'Square-Version': squareVersion,
                'Content-Type': `application/json`
                },
            body: {
                'client_id': squareApplicationId,
                'client_secret': squareApplicationSecret,
                "code": code,
                "grant_type": "authorization_code"
                }
        };

        /* handle callback result */
        function callback(error, response, body) {
            if (!error && (response.statusCode == 200) && body != null) {
                resolve(body)
            } else { 
                reject(error || "Something went wrong when processing the payment. Please try again later.");
            }
        }

        /* make internal request, set auth headers */
        request.post(options, callback);
    });
}

module.exports = {
    authorize: authorize,
    obtainToken: obtainToken
}