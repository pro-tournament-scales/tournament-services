const mongojs = require("mongojs");
const db = require("../common/dbConnection").db;

function updateTournament(tournament) {
    console.log("backing up tournament! ")

    return new Promise(function(resolve, reject) {
        let id = tournament._id; /* capture immutable field */
        console.log("id = " + tournament._id);
        delete tournament._id; /* remove immutable field from obj */
        console.log("deleted id");
        db.tournamentApp.findAndModify({query: {_id: id},
        update: {$set: tournament},
        upsert: true,
        new: true}, function(err, resp) {
            if (err) { 
                console.log("error! " + err);
                reject(err); return; 
            }
            console.log("success! " + resp)
            resolve(resp); 
        });
    });
}

function deleteTournament(id) {
    return new Promise(function(resolve, reject) {
        db.tournamentApp.remove({_id: id}, function(err, resp) {
            if (err) { reject(err); return; }
            resolve(resp);
        });    
    });
}


function getTournament(tournamentId) {
    return new Promise(function(resolve, reject) {
        db.tournamentApp.findOne({ _id: tournamentId}, function(err, resp) {
            if (err) { reject(err); return; }
            resolve(resp);
        })
    })
}

function getAllTournaments() {
    return new Promise(function(resolve, reject) {
        db.tournamentApp.find({}, function(err, resp) {
            if (err) { reject(err); return; }
            resolve(resp);
        })
    })
}


module.exports = {
    updateTournament: updateTournament,
    deleteTournament: deleteTournament,
    getTournament: getTournament,
    getAllTournaments: getAllTournaments
}