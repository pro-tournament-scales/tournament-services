const mongojs = require("mongojs");

function CommonTournamentCollection(dbCollection, 
    transform = function(tournament) { return tournament; }) { 

    this.createTournament = function(tournament) {
        return new Promise(function(resolve, reject) {
            dbCollection.insert(tournament, function(err, resp) {
                if (err) { reject(err); return; }
                resolve(transform(resp));
            });
        });
    }
    
    this.updateTournament = function(tournament) {
        return new Promise(function(resolve, reject) {
            let id = tournament._id; /* capture immutable field */
            delete tournament._id; /* remove immutable field from obj */
            dbCollection.findAndModify({query: {_id: mongojs.ObjectId(id)},
            update: {$set: tournament},
            new: true}, function(err, resp) {
                if (err) { reject(err); return; }
                resolve(transform(resp));
            });
        });
    }
    
    this.updateTournamentRegistrationStatus = function(id, isOpen) {
        return new Promise(function(resolve, reject) {
            dbCollection.findAndModify({query: {_id: mongojs.ObjectId(id)},
            update: {$set: { registrationOpen: isOpen }},
            new: true}, function(err, resp) {
                if (err) { reject(err); return; }
                resolve(transform(resp));
            });
        });
    }
    
    this.deleteTournament = function(id) {
        return new Promise(function(resolve, reject) {
            dbCollection.remove({_id: mongojs.ObjectId(id)}, function(err, resp) {
                if (err) { reject(err); return; }
                resolve(transform(resp));
            });    
        });
    }
    
    this.getTournaments = function() {
        return new Promise(function(resolve, reject) {
            dbCollection.find({}, function(err, resp) {
                if (err) { reject(err); return; }
                resp = resp.map(tournament => {
                    if (tournament.registrationOpen == null) {
                        tournament.registrationOpen = true;
                    }
                    return tournament;
                });
                resolve(transform(resp));
            });
        });
    }
    
    this.getTournamentsById = function(ids) {
        return new Promise(function(resolve, reject) {
            dbCollection.find({ _id: { $in: ids.map(id => mongojs.ObjectId(id)) } }, function(err, resp) {
                if (err) { reject(err); return; }
                resp = resp.map(tournament => {
                    if (tournament.registrationOpen == null) {
                        tournament.registrationOpen = true;
                    }
                    return tournament;
                });
                resolve(transform(resp));
            });
        });
    }

    this.getTournament = function(tournamentId) {
        return new Promise(function(resolve, reject) {
            dbCollection.findOne({ _id: mongojs.ObjectId(tournamentId)}, function(err, resp) {
                if (err) { reject(err); return; }
                if (resp != null && resp.registrationOpen == null) {
                    resp.registrationOpen = true;
                }
                resolve(transform(resp));
            })
        })
    }
    
    this.getTournamentsForDivision = function(divisionId) {
        return new Promise(function(resolve, reject) {
            dbCollection.find({ divisionId: divisionId}, function(err, resp) {
                if (err) { reject(err); return; }
                resp = resp.map(tournament => {
                    if (tournament.registrationOpen == null) {
                        tournament.registrationOpen = true;
                    }
                    return tournament;
                });
                resolve(transform(resp));
            })
        })
    }
    
    this.getUpcomingTournaments = function(date) {
        return new Promise(function(resolve, reject) {
            dbCollection.find({ endTimestamp: { $gte: date } }, function(err, resp) {
                if (err) { reject(err); return; }
                resp = resp.map(tournament => {
                    if (tournament.registrationOpen == null) {
                        tournament.registrationOpen = true;
                    }
                    return tournament;
                });
                resolve(transform(resp));
            })
        })
    }
    
    
    this.getPastTournaments = function(date) {
        return new Promise(function(resolve, reject) {
            dbCollection.find({ endTimestamp: { $lte: date } }, function(err, resp) {
                if (err) { reject(err); return; }
                resp = resp.map(tournament => {
                    if (tournament.registrationOpen == null) {
                        tournament.registrationOpen = true;
                    }
                    return tournament;
                });
                resolve(transform(resp));
            })
        })
    }
    
    this.updateTournamentDetailsUrl = function(id, detailsUrl) {
        return new Promise(function(resolve, reject) {
            dbCollection.findAndModify({query: {_id: mongojs.ObjectId(id)},
            update: {$set: { detailsUrl: detailsUrl }},
            new: true}, function(err, resp) {
                if (err) { reject(err); return; }
                resolve(transform(resp));
            });
        });
    }
    
    this.updateTournamentResultsUrl = function(id, resultsUrl) {
        return new Promise(function(resolve, reject) {
            dbCollection.findAndModify({query: {_id: mongojs.ObjectId(id)},
            update: {$set: { resultsUrl: resultsUrl }},
            new: true}, function(err, resp) {
                if (err) { reject(err); return; }
                resolve(transform(resp));
            });
        });
    }
    
    this.updateTournamentHighschoolResultsUrl = function(id, highschoolResultsUrl) {
        return new Promise(function(resolve, reject) {
            dbCollection.findAndModify({query: {_id: mongojs.ObjectId(id)},
            update: {$set: { highschoolResultsUrl: highschoolResultsUrl }},
            new: true}, function(err, resp) {
                if (err) { reject(err); return; }
                resolve(transform(resp));
            });
        });
    }
    
    this.getUpcomingTournamentsForDivision = function(divisionId, date) {
        return new Promise(function(resolve, reject) {
            dbCollection.find({ divisionId: divisionId, endTimestamp: { $gte: date } }, function(err, resp) {
                if (err) { reject(err); return; }
                resp = resp.map(tournament => {
                    if (tournament.registrationOpen == null) {
                        tournament.registrationOpen = true;
                    }
                    return tournament;
                });
                resolve(transform(resp));
            })
        })
    }
    
    this.getPastTournamentsForDivision = function(divisionId, date) {
        return new Promise(function(resolve, reject) {
            dbCollection.find({ divisionId: divisionId, endTimestamp: { $lte: date } }, function(err, resp) {
                if (err) { reject(err); return; }
                resp = resp.map(tournament => {
                    if (tournament.registrationOpen == null) {
                        tournament.registrationOpen = true;
                    }
                    return tournament;
                });
                resolve(transform(resp));
            })
        })
    }
}

module.exports = CommonTournamentCollection