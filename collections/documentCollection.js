const mongojs = require("mongojs");
const db = require("../common/dbConnection").db;

function getDocuments(userId) {
    return new Promise(function(resolve, reject) {
        db.document.findOne({userId: userId}, function(err, resp) {
            if (err) { reject(err); return; }
            resolve(resp);
        });
    });
}

function createDocument(userId, path) {
    return new Promise(function(resolve, reject) {
        db.document.insert({userId: userId, paths: [path]}, function(err, resp) {
            if (err) { reject(err); return; }
            resolve(resp);
        })
    });
}

function updateDocuments(userId, paths) {
    return new Promise(function(resolve, reject) {
        db.document.findAndModify({query: {userId: userId},
        update: {$set: { paths: paths }},
        new: true}, function(err, resp) {
            if (err) { reject(err); return; }
            resolve(resp); 
        });
    });
}

function deleteDocument(id) {
    return new Promise(function(resolve, reject) {
        db.document.remove({_id: mongojs.ObjectId(id)}, function(err, resp) {
            if (err) { reject(err); return; }
            resolve(resp);
        }); 
    });
}

module.exports = {
    getDocuments: getDocuments,
    createDocument: createDocument,
    updateDocuments: updateDocuments,
    deleteDocument: deleteDocument
}