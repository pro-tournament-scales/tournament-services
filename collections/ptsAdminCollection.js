const mongojs = require("mongojs");
const db = require("../common/dbConnection").db;

function createPTSAdmin(PTS) {
    return new Promise(function(resolve, reject) {
        db.PTSAdmin.insert(PTS, function(err, resp) {
            if (err) { reject(err); return; }
            resolve(resp);
        });
    });
}

function findPTSAdminByEmail(email) {
    return new Promise(function(resolve, reject) {
        db.PTSAdmin.findOne({email: { $regex: '^' + email + '$',  $options: 'i'} }, function(err, resp) {
                if (err) { reject(err); return; }
                resolve(resp);
        });
    });
}

function findPTSAdmin(id) {
    return new Promise(function(resolve, reject) {
        db.PTSAdmin.findOne({_id: mongojs.ObjectId(id)}, function(err, resp) {
            if (err) { reject(err); return; }
            resolve(resp);
        });
    });
}

function getAdmins() {
    return new Promise(function(resolve, reject) {
        db.PTSAdmin.find({}, function(err, resp) {
            if (err) { reject(err); return; }
            resolve(resp);
        });
    });
}

module.exports = {
    createPTSAdmin: createPTSAdmin,
    findPTSAdminByEmail: findPTSAdminByEmail,
    findPTSAdmin: findPTSAdmin,
    getAdmins: getAdmins
}