const mongojs = require("mongojs");
const db = require("../common/dbConnection").db;


function getActiveCampaigns() {
    return new Promise(function(resolve, reject) {
        let now = new Date()
        console.log("comparing date - " + now)
        //'betaCampaigns.startDate': {$lte : now}, 'betaCampaigns.endDate': {$gte : now}
        db.betaCampaigns.find({startDate: {$lte: now}, endDate: {$gte: now}}, function(err, resp) {
            if (err) { 
                console.log(err);
                reject(err); return;
             }
             

            resolve(resp);
        })
    })
}

module.exports = {
    getActiveCampaigns : getActiveCampaigns
};
