const mongojs = require("mongojs");

function CommonTeamCollection(dbCollection, 
    transform = function(tournament) { return tournament; }) { 
        this.createTeam = function(team) {
            return new Promise(function(resolve, reject) {
                dbCollection.insert(team, function(err, resp) {
                    if (err) { reject(err); return; }
                    resolve(resp);
                });
            });
        }
        
        this.updateTeam = function(team) {
            return new Promise(function(resolve, reject) {
                let id = team._id;
                /* update team positions only */
                let updatedTeam = { primaryAngler: team.primaryAngler }
                updatedTeam.secondaryAngler = team.secondaryAngler
                updatedTeam.observer = team.observer
                if (team.boatCaptain != null) { updatedTeam.boatCaptain = team.boatCaptain; }
        
                dbCollection.findAndModify({query: {_id: mongojs.ObjectId(id)},
                update: { $set: updatedTeam },
                new: true}, function(err, resp) {
                    if (err) { reject(err); return; }
                    resolve(resp);
                });
            })
        }
        
        this.updateTeamRemoveSecondaryAngler = function(teamId) {
            return new Promise(function(resolve, reject) {
                dbCollection.findAndModify({query: {_id: mongojs.ObjectId(teamId)},
                update: { $unset: { secondaryAngler: 1} },
                new: true}, function(err, resp) {
                    if (err) { reject(err); return; }
                    resolve(resp);
                });
            })
        }
        
        this.updateTeamRemoveBoatCaptain = function(teamId) {
            return new Promise(function(resolve, reject) {
                dbCollection.findAndModify({query: {_id: mongojs.ObjectId(teamId)},
                update: { $unset: { boatCaptain: 1} },
                new: true}, function(err, resp) {
                    if (err) { reject(err); return; }
                    resolve(resp);
                });
            })
        }
        
        this.updateTeamRemoveObserver = function(teamId) {
            return new Promise(function(resolve, reject) {
                dbCollection.findAndModify({query: {_id: mongojs.ObjectId(teamId)},
                update: { $unset: { observer: 1} },
                new: true}, function(err, resp) {
                    if (err) { reject(err); return; }
                    resolve(resp);
                });
            })
        }
        
        this.updatePaidStatus = function(id, status) {
            return new Promise(function(resolve, reject) {
                dbCollection.findAndModify({query: {_id: mongojs.ObjectId(id)},
                update: {$set: {paidStatus: status}},
                new: true}, function(err, resp) {
                    if (err) { reject(err); return; }
                    resolve(resp);
                });
            });
        }
        
        this.getTeam = function(id) {
            return new Promise(function(resolve, reject) {
                dbCollection.findOne({ _id: mongojs.ObjectId(id) }, function(err, resp) {
                    if (err) { reject(err); return; }
                    resolve(resp);
                })
            })
        }
        
        this.getTeams = function() {
            return new Promise(function(resolve, reject) {
                dbCollection.find({ }, function(err, resp) {
                    if (err) { reject(err); return; }
                    resolve(resp);
                })
            });
        }

        this.getTeamsForUser = function(userId) { 
            return new Promise(function(resolve, reject) {
                dbCollection.find({ $or: [ { 'primaryAngler.id': userId }, { 'secondaryAngler.id': userId }, { 'boatCaptain.id': userId }, { 'observer.id': userId } ] }, function(err, resp) {
                    if (err) { reject(err); return; }
                    resolve(resp);
                })
            });
        }
        
        this.deleteTeam = function(id) {
            return new Promise(function(resolve, reject) {
                dbCollection.remove({_id: mongojs.ObjectId(id)}, function(err, resp) {
                    if (err) { reject(err); return; }
                    resolve(resp);
                });    
            });
        }
        
        this.getTeamForPrimaryAngler = function(anglerId, tournamentId) {
            return new Promise(function(resolve, reject) { 
                dbCollection.findOne({'primaryAngler.id': anglerId, tournamentId: tournamentId}, function(err, resp) {
                    if (err) { reject(err); return; }
                    resolve(resp);
                });
            });
        }
        
        this.getTeamsForTournament = function(tournamentId) {
            return new Promise(function(resolve, reject) {
                dbCollection.find({tournamentId: tournamentId}, function(err, resp) {
                    if (err) { reject(err); return; }
                    resolve(resp);
                });
            });
        }
}

module.exports = CommonTeamCollection