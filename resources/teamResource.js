const jwt = require('../authentication/jwt');
const controller = require("../controllers/teamController");
const userVerifyMiddleware = require("../authentication/middleware/userVerifyMiddleware");
const adminVerifyMiddleware = require("../authentication/middleware/adminVerifyMiddleware");
module.exports = function(app) {
    app.use("/v1/team", jwt.user());
    app.use("/v1/teams", jwt.user());
    app.use('/v1/pts/team', jwt.admin());

    app.post("/v1/team",
    userVerifyMiddleware.verifyAngler,
    function(req, res) {
        controller.createTeam(req.body)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message}); });
    });

    app.put("/v1/team",
    userVerifyMiddleware.verifyAngler,
    function(req, res) {
        controller.updateTeam(req.body)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message}); });
    });

    app.get("/v1/teams/torunament/:tournamentId",
    function(req, res) {
        controller.getTeamsForTournament(req.params.tournamentId)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message }); })
    })

    app.get("/v1/teams", 
    function(req, res) {
        controller.getTeams(req.user.id)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message }); })
    })

    app.get("/v1/teams/archived", 
    function(req, res) {
        controller.getArchivedTeams(req.user.id)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message }); })
    })

    app.get("/v1/teams/highschool/:highschoolId",
    function(req, res) {
        controller.getTeamsForHighschool(req.params.highschoolId)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message }); })
    });

    app.get("/v1/pts/teams", 
    function(req, res) {
        controller.getTeams(req.query.userid)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message }); })
    })

    app.get("/v1/pts/teams/archived", 
    function(req, res) {
        controller.getArchivedTeams(req.query.userid)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message }); })
    })

    app.get("/v1/pts/teams/tournament/:tournamentId",
    function(req, res) {
        controller.getTeamsForTournament(req.params.tournamentId)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message }); })
    })

    /* require angler or highschool admin */
    app.post("/v1/team/register/:teamId",
    userVerifyMiddleware.requireAnglerOrHighschoolAdmin,
    function(req, res) {
        controller.registerTeam(req.params.teamId, req.body.cardNonce, req.user.email)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message }); })
    })

    /* pay for multiple teams  */
    app.get('/v1/team/cost/:teamId',
    function(req, res) {
        if (req.params.teamId == null) { res.status(200).json({ total: 0, subtotal: 0}); return; }
        require('../controllers/teamController').getTeamWithFee(req.params.teamId)
        .then(function(result) { 
            let subtotal = result.tournamentFee.late_fee_subtotal || result.tournamentFee.subtotal;
            let total = require('../square/payer').calculateCostWithSquare(subtotal);
            res.status(200).json({
                total: total / 100,
                subtotal: subtotal,
                status: result.tournamentFee.status
            }); 
        })
        .catch(function(error) { res.status(error.status).json({ message: error.message}); });
    })

    app.delete("/v1/pts/team/:teamId",
    adminVerifyMiddleware.requireWriteAdmin,
    function(req, res) {
        controller.deleteTeam(req.params.teamId)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message }); })
    })

    app.put("/v1/pts/team/:teamId/status/:paidStatus",
    adminVerifyMiddleware.requireWriteAdmin,
    function(req, res) {
        controller.updateTeamPaidStatus(req.params.teamId, req.params.paidStatus)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message }); })
    })
}