const jwt = require('../authentication/jwt');
const adminMiddleware = require("../authentication/middleware/adminVerifyMiddleware");
const uploadMiddleware = require("../authentication/middleware/uploadMiddleware");
const controller = require("../controllers/tournamentController")
const archiveController = require("../controllers/legacyResultsArchiveController")

module.exports = function(app) {
    app.use("/v1/pts/tournament", jwt.admin());
    app.use("/v1/tournament", jwt.user());

    app.post("/v1/pts/tournament",
    adminMiddleware.requireWriteAdmin,
    function(req, res) {
        controller.createTournament(req.body)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message}); });
    });

    app.put("/v1/pts/tournament",
    adminMiddleware.requireWriteAdmin,
    function(req, res) {
        controller.updateTournament(req.body)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message}); });
    })

    app.delete("/v1/pts/tournament/:tournamentId", 
    adminMiddleware.requireWriteAdmin,
    function(req, res) {
        controller.deleteTournament(req.params.tournamentId)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message}); });
    })

    app.get("/v1/pts/tournaments/division/:divisionId",
    adminMiddleware.requireReadAdmin,
    function(req, res) {
        controller.getTournamentsForDivision(req.params.divisionId)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message}); });
    });
    
    app.put("/v1/pts/tournament/:id/upload-result",
    [adminMiddleware.requireWriteAdmin, uploadMiddleware.multer.single("tournamentResults")],
    function(req, res) {
        controller.uploadTournamentResults(req.params.id, req.file)
        .then(result => { res.status(result.status).json(result.body); })
        .catch(error => { res.status(error.status).json({ message: error.message }); })
    })

    app.put("/v1/pts/tournament/:id/upload-highschool-result",
    [adminMiddleware.requireWriteAdmin, uploadMiddleware.multer.single("tournamentHighschoolResults")],
    function(req, res) {
        controller.uploadTournamentHighschoolResults(req.params.id, req.file)
        .then(result => { res.status(result.status).json(result.body); })
        .catch(error => { res.status(error.status).json({ message: error.message }); })
    })

    app.put("/v1/pts/tournament/:id/upload-details",
    [adminMiddleware.requireWriteAdmin, uploadMiddleware.multer.single("tournamentDetails")],
    function(req, res) {
        controller.uploadTournamentDetails(req.params.id, req.file)
        .then(result => { res.status(result.status).json(result.body); })
        .catch(error => { res.status(error.status).json({ message: error.message }); })
    })

    app.put("/v1/pts/tournament/:id/registration-open/:status",
    adminMiddleware.requireWriteAdmin,
    function(req, res) {
        if (req.params.status == null) { res.status(400).json({message: "Invalid registration status. Please try again."}); return; }
        let status = req.params.status == "true";
        controller.updateTournamentRegistrationStatus(req.params.id, status)
        .then(result => res.status(result.status).json(result.body))
        .catch(error => res.status(error.status).json({message: error.message}));
    })

    app.get("/v1/pts/tournaments/upcoming/division/:divisionId",
    adminMiddleware.requireReadAdmin,
    function(req, res) {
        controller.getUpcomingTournamentsForDivision(req.params.divisionId)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message }); })
    });

    app.get("/v1/tournaments/division/:divisionId",
    function(req, res) {
        controller.getTournamentsForDivision(req.params.divisionId)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message}); });
    });

    app.get("/v1/tournaments/upcoming",
    function(req, res) {
        controller.getUpcomingTournaments()
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message }); })
    })

    app.get("/v1/tournaments/upcoming/division/:divisionId",
    function(req, res) {
        controller.getUpcomingTournamentsForDivision(req.params.divisionId)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message }); })
    })

    app.get("/v1/tournaments/past",
    function(req, res) {
        controller.getPastTournaments()
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message }); })
    })

    app.get("/v1/tournaments/archive",
    function(req, res) {
        controller.getArchivedTournaments()
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message }); })
    })

    app.get("/v1/tournaments/past/division/:divisionId",
    function(req, res) {
        controller.getPastTournamentsForDivision(req.params.divisionId)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message }); })
    })

    // archive resources

    app.get("/v1/tournaments/archive/teams",
    function(req, res) {
        archiveController.getArchivedTeamResults()
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message }); })
    })

    app.get("/v1/tournaments/archive/schools",
    function(req, res) {
        archiveController.getArchivedSchoolResults()
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message }); })
    })

    // tournament software resources
   
    app.put("/v1/pts/tournament/app/upload-tournament-data",
     adminMiddleware.requireWriteAdmin,
     function(req, res) {
         console.log("starting backup...");
         controller.uploadAppTournamentData(req.body)
         .then(result => { res.status(result.status).json(result.body); })
         .catch(error => { res.status(error.status).json({ message: error.message }); })
    })

    app.get("/v1/pts/tournament/app/tournament-data",
     adminMiddleware.requireReadAdmin,
     function(req, res) {
         controller.getAppTournamentData()
         .then(result => { res.status(result.status).json(result.body); })
         .catch(error => { res.status(error.status).json({ message: error.message }); })
    })

    app.put("/v1/pts/tournament/app/upload-results",
    adminMiddleware.requireWriteAdmin,
    function(req, res) {
        controller.uploadResultsData(req.body)
        .then(result => { res.status(result.status).json(result.body); })
        .catch(error => { res.status(error.status).json({ message: error.message }); })
    })


    app.get("/v1/pts/tournament/:id/app/tournament-data",
    adminMiddleware.requireReadAdmin,
    function(req, res) {
        controller.getAppTournamentData(req.params.id)
        .then(result => { res.status(result.status).json(result.body); })
        .catch(error => { res.status(error.status).json({ message: error.message }); })
   })

}