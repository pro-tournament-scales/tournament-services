const controller = require("../controllers/betaController");
const jwt = require('../authentication/jwt');
const userVerifyMiddleware = require("../authentication/middleware/userVerifyMiddleware");
module.exports = function(app) {

    app.use("/v1/beta", jwt.user());
    
    app.get('/v1/beta/campaigns',
    userVerifyMiddleware.verifyBetaParticipant,
    function(req, res) {
        console.log("getting beta campaigns")
        controller.getActiveCampaigns()
        .then(function(response) { res.status(response.status).json(response.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message }); });
    })

    app.get('/v1/beta/campaign/:campaignId',
    userVerifyMiddleware.verifyBetaParticipant,
    function(req, res) {
        controller.broadcastMessage(req.params.topic, req.body.title, req.body.message)
        .then(function(response) { res.status(response.status).json(response.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message }); });
    })
}