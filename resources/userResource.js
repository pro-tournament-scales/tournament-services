const jwt = require('../authentication/jwt');
const uploadMiddleware = require("../authentication/middleware/uploadMiddleware");
const userMiddleware = require("../authentication/middleware/userVerifyMiddleware");
const adminMiddleware = require("../authentication/middleware/adminVerifyMiddleware");
const basicAuthMiddleware = require("../authentication/middleware/basicAuthMiddleware");
const controller = require("../common/userController");
const domainHost = process.env.HOST;

module.exports = function(app) {

    app.get("/v1/user/login",
    basicAuthMiddleware.handleBasicAuth,
    function(req, res) {
        console.log('attempting login!');
        controller.authenticate(req.headers.email, req.headers.password)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(500).json({ message: error.message}); });
    });

    app.get('/v1/user/encrypt/:password', function(req, res) {
        controller.encryptPassword(req.params.password)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(500).json({ message: error.message}); });
    });


    // app.post("/v1/user/create",
    //     function(req, res) {
    //         controller.createUser(req.body)
    //         .then(function(result) { res.status(result.status).json(result.body); })
    //         .catch(function(error) { res.status(error.status).json({ message: error.message}); });
    // });
}
