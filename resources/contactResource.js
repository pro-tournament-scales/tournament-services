const contactController = require('../controllers/contactController');
const jwt = require('../authentication/jwt');

module.exports = function(app) { 
    app.use("/v1/contact", jwt.user());
    app.post("/v1/contact/enquire", function(req, res) {
        contactController.generalEnquiry(req.body)
        .then(function(result) { res.status(result.status).json(result.body); })
        .catch(function(error) { res.status(error.status).json({ message: error.message}); });
    });
}